<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php 
        include_once('libs/PhotoCaption/ThaanaConversions.php');  
        $ID = $post->ID;
        echo bloginfo('title'). ' - ' /*. Thaana_Transliterator::transliterate($post->post_title)*/; 
        the_post_transliteration($ID);?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/app.css?v=1.1">
    <?php
    /**
     * The template part for displaying single posts
     *
     * @package WordPress
     * @subpackage Twenty_Sixteen
     * @since Kulhudhufushi
     */
    global $image_title;
    $image_title = get_the_post_thumbnail_caption();

    ?>
<meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Kulhudhufushi.com">
    <meta name="twitter:title" content="<?php the_post_transliteration($ID); ?>">
    <meta name="twitter:description" content="<?php the_post_transliteration($ID); ?>">
    <meta name="twitter:creator" content="@kulhudhuffushi">
    <meta name="twitter:image:src" content="<?php echo get_post_meta($post->ID,'kulhudhu_social_image',true); ?>">
    <meta name="twitter:domain" content="www.kulhudhuffushi.com">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Kulhudhufushi Online">
    <meta property="og:title" content="<?php the_post_transliteration($ID); ?>">
    <meta property="og:description" content="<?php the_post_transliteration($ID); ?>">
    <meta property="og:url" content="<?php the_permalink(); ?>">
    <meta property="og:locale" content="en_US">
    <meta property="og:image" content="<?php echo get_post_meta($post->ID,'kulhudhu_social_image',true); ?>">
    <link rel="shortcut icon" type="image/gif" href="<?php echo get_template_directory_uri().'/images/fav-ico.gif'; ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <style type="text/css">
        @media (max-width: 992px)
        {
            
            ul.share {
                list-style: none;
                margin-right: -50px;
            }
            ul.share li {
                display: inline-block;
            }

            ul.share.affix {
                top: 50px;
                margin-left: 70px;
                position: initial;
            }

            ul.share img {
                 margin: 0; 
            }
        }
    </style>
</head>
<body>
    <img style="width: 1px;height: 1px; position: absolute;" src='<?php echo get_post_meta($post->ID,'kulhudhu_social_image',true); ?>'/>

<div class="wrapper">
    <div class="top-header"></div>
    <div class="jumbotron top-bar container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-4 logo-container pull-left">
                    <a href="/">
                        <img class="logo pull-left" width="200px" src="<?php echo get_template_directory_uri() ?>/images/k-online.png">
                    </a>
                </div>

                <div class="col-md-8">
                    <div class="header-ad" >
                        <!-- style="width: 590px; height: 100px" -->
                        <?php include_once("template-parts/partials/home/home_top_advertisement.php"); ?>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <?php include('template-parts/partials/nav.php'); ?>
        </div>
    </div>
    <div class="container fixed-stage">


