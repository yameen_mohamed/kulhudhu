<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<hr>



<div id="comments" class="comments-area">

<div>
			<button type="button" data-parent=0 class="btn btn-primary btn-lg waheed" data-toggle="modal" data-target="#commentModal">
			  <i class="glyphicon glyphicon-bullhorn"></i>
			    ކޮމެންޓް ކުރުމަށް
			</button>
			<br>
</div>

	<?php if ( have_comments() ) : ?>

		<?php the_comments_navigation(); ?>



		<ol class="comment-list media-list">
			<?php

			function twbs_comment_format($comment, $args, $depth) {
			$GLOBALS['comment'] = $comment; 

				if($comment->comment_approved == '0') return;
			?>

			<li class="comment-box parent">
				<div class="comment-header">
					<span class="icon">
						<?php echo get_avatar( $comment, 26 ); ?>
					</span>
					<span class="caption author"><?php comment_author_link(); ?></span>
					<span class="caption time">
						<?php echo \Carbon\Carbon::parse($comment->comment_date)->diffForHumans(); ?>
					</span>
				</div>
				<div class="comment-body">
					<?php echo ($comment->comment_content) ?>
				</div>
				<div class="comment-footer">
					<?php if($comment->comment_parent == 0): ?>
					<div class="reply clickable" data-author='<?php 
						if ( $comment->user_id > 0 && $user = get_userdata( $comment->user_id ) ) {
							echo sanitize_html_class( $user->user_nicename, $comment->user_id );
						}
						else
							echo count($comment->children);
					 ?>' data-parent='<?php echo $comment->comment_ID ?>' data-toggle="modal" data-target="#commentModal">
						<a href="javascript:void()" >
							<i class="glyphicon glyphicon-send"></i>
						</a>
						
					</div>
				<?php endif;?>
					<div class="emotions">
						<span class="button clickable like">
							<a class="btn-emtion" data-emotion="like"  data-commentid="<?php echo  $comment->comment_ID; ?>"><i class="glyphicon glyphicon-thumbs-up"></i>
	                                        <small id="like_comment<?php echo $comment->comment_ID; ?>" class="likes"><?php echo get_comment_meta($comment->comment_ID,'likes', true) ?></small>

	                        </a>
						</span>
						<span class="button clickable dislike">
                            <a class="btn-emtion" data-emotion="dislike"  data-commentid="<?php echo  $comment->comment_ID; ?>"><i class="glyphicon glyphicon-thumbs-down"></i>
                                    <small id="dislike_comment<?php echo $comment->comment_ID; ?>" class="likes"><?php echo get_comment_meta($comment->comment_ID,'dislikes', true) ?></small>
                            </a>
						</span>
						<span class="button clickable dislike">
                            <a class="btn-emtion" data-emotion="love"  data-commentid="<?php echo  $comment->comment_ID; ?>"><i class="glyphicon glyphicon-heart"></i>
                                    <small id="love_comment<?php echo $comment->comment_ID; ?>" class="likes"><?php echo get_comment_meta($comment->comment_ID,'love', true) ?></small>
                            </a>
						</span>
					</div>
				</div>

			</li>
		
				<?php 
			} // twbs_comment_format

				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 42,
					'callback'=>'twbs_comment_format'
				));
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation(); ?>

	<?php endif; // Check for have_comments(). ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments">ކޮމެންޓް ކުރުންވަނީ ބަންދު ކުރެވިފައި</p>
	<?php endif; ?>

	<?php if( isset( $_GET['thankyou'] ) ) { ?>
		<div class="thankyou" style="border: 1px solid red; padding: 20px;">
			<p class="waheed">ކުޑައިރުކޮޅެއްގެ ތެރޭގައި ތިޔަ ކޮމެންޓް ޝާއިޢު ކުރެވޭނެ</p>
		</div>
        <?php } ?>

	<?php

	$comments_args = array(
		'fields'=>array(
			// 'email'=>'<div class="form-group"><label>އީމެއިލް</label><input dir="rtl" class="form-control" type="email" required name="email" id="email" placeholder="އީމެއިލް ޖައްސަވާ" /></div>',
//			'author'=>'<div class="form-group"><label>ނަން</label><input dir="rtl" class="form-control" type="text" required name="author" id="author" placeholder="ނަން ޖއްސަވާ" /></div>',
		),
		// change the title of send button
		'label_submit'=>'Proceed',
		'class_submit' => 'hide',
		// change the title of the reply section
		'title_reply'=> '<div/>',
		'id_form'=>'comment_form',
		'comment_notes_before'=>' ',
		// remove "Text or HTML to be displayed after the set of comment fields"
		'comment_notes_after' => '',
		'title_reply'		=> '',
		'title_reply_to' => '<i class="glyphicon glyphicon-send"></i> %s އަށް ރައްދު ދިނުމަށް ',
		'title_reply_before' => '<h3 id="reply-title" class="waheed comment-reply-title">',
		'cancel_reply_link' => '<i class="glyphicon glyphicon-ban-circle"></i>  ކެންސަލް',
		'cancel_reply_before' => '<h3 class="waheed comment-reply-title text-sm">',

		// redefine your own textarea (the comment body)
		'comment_field' =>  '<input type="hidden" name="email" id="email" value="dummy@dummys.com">
			<div class="form-group"><label for="author">ނަން</label><input type="text" id="author" class="thaana form-control" placeholder="ނަން" name="author" aria-required="true"/>
			</div>
			<div class="form-group"><textarea id="comment" class="form-control thaana" rows="5" placeholder="ޚިޔާލު" name="comment" aria-required="true"></textarea>
			</div>
			<button class="btn btn-sm btn-success faseyha" type="submit">ފޮނުވާ</button>',
	);

	// comment_form($comments_args);

	?>

	
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title waheed" id="commentModalLabel">ކޮމެންޓްކުރުމަށް</h4>
      </div>
      <form action="/wp-comments-post.php" method="post" id="comment_form_model" class="comment-form">
      	<div class="modal-body">
      		<input type="hidden" name="email" id="email" value="dummy@dummys.com">
        	<div class="form-group is-empty">
        		<label for="author">ނަން</label>
        		<input type="text" id="author" class="thaana form-control" placeholder="ނަން" name="author" aria-required="true">
				<span class="material-input"></span>
			</div>
			<div class="form-group is-empty">
				<textarea id="comment" class="form-control thaana" rows="5" placeholder="ޚިޔާލު" name="comment" aria-required="true"></textarea>
				<span class="material-input"></span>
			</div>
			<?php wp_comment_form_unfiltered_html_nonce() ?>
			<input type="hidden" name="comment_post_ID" value="<?php echo $post->ID ?>">
			<input type="hidden" name="comment_parent" id="comment_parent" value="0">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default waheed" data-dismiss="modal">ކެންސަލް</button>
        <button type="submit" class="btn btn-primary waheed">ކޮމެންޓް</button>
      </div>
			</form>

    </div>
  </div>
</div>
</div>