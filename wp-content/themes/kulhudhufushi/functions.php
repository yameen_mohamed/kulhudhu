<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

include_once 'wp_bootstrap_walker.php';
require_once 'libs/Advertisement.php';
require_once 'libs/Ishthihaaru/Ishthihaaru.php';
require_once 'libs/youtube/YouTube.php';
require_once 'libs/gallery/Gallery.php';
require_once 'vendor/autoload.php';
require_once 'libs/NewsModifier/NewsModifier.php';
require_once 'libs/latest_news/LatestNews.php';
require_once 'libs/PhotoCaption/PhotoCaption.php';


date_default_timezone_set('Indian/Maldives');
/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'kulhudhufushi_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own kulhudhufushi_setup() function to override in a child theme.
 *
 * @since Kulhudhufushi
 */
function kulhudhufushi_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'kulhudhufushi' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'kulhudhufushi', get_stylesheet_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'kulhudhufushi' ),
		'social'  => __( 'Social Links Menu', 'kulhudhufushi' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );



	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', kulhudhufushi_fonts_url() ) );
}
endif; // kulhudhufushi_setup
add_action( 'after_setup_theme', 'kulhudhufushi_setup' );




/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Kulhudhufushi
 */
function kulhudhufushi_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kulhudhufushi_content_width', 840 );
}
add_action( 'after_setup_theme', 'kulhudhufushi_content_width', 0 );

add_action( 'admin_enqueue_scripts', 'enqueue_admin_js' );

function enqueue_admin_js()
{
	wp_enqueue_script('app', get_template_directory_uri() . '/js/app.js');
}


/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Kulhudhufushi
 */
function kulhudhufushi_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'kulhudhufushi' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'kulhudhufushi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 1', 'kulhudhufushi' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'kulhudhufushi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Content Bottom 2', 'kulhudhufushi' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Appears at the bottom of the content on posts and pages.', 'kulhudhufushi' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'kulhudhufushi_widgets_init' );

if ( ! function_exists( 'kulhudhufushi_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own kulhudhufushi_fonts_url() function to override in a child theme.
 *
 * @since Kulhudhufushi
 *
 * @return string Google fonts URL for the theme.
 */
function kulhudhufushi_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'kulhudhufushi' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'kulhudhufushi' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'kulhudhufushi' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Kulhudhufushi
 */
function kulhudhufushi_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'kulhudhufushi_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Kulhudhufushi
 */
function kulhudhufushi_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'kulhudhufushi-fonts', kulhudhufushi_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'kulhudhufushi-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'kulhudhufushi-ie', get_template_directory_uri() . '/css/ie.css', array( 'kulhudhufushi-style' ), '20150930' );
	wp_style_add_data( 'kulhudhufushi-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'kulhudhufushi-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'kulhudhufushi-style' ), '20151230' );
	wp_style_add_data( 'kulhudhufushi-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'kulhudhufushi-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'kulhudhufushi-style' ), '20150930' );
	wp_style_add_data( 'kulhudhufushi-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'kulhudhufushi-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'kulhudhufushi-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'kulhudhufushi-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151112', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'kulhudhufushi-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20151104' );
	}

	wp_enqueue_script( 'kulhudhufushi-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20151204', true );

	wp_localize_script( 'kulhudhufushi-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'kulhudhufushi' ),
		'collapse' => __( 'collapse child menu', 'kulhudhufushi' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'kulhudhufushi_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Kulhudhufushi
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function kulhudhufushi_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'kulhudhufushi_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Kulhudhufushi
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function kulhudhufushi_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Kulhudhufushi
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function kulhudhufushi_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'kulhudhufushi_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Kulhudhufushi
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function kulhudhufushi_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'kulhudhufushi_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function kulhudhufushi_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'kulhudhufushi_widget_tag_cloud_args' );

function get_title() {
	if ( is_category() ) {
		$title = sprintf( __( 'ކެޓެގަރީ: %s' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'ޓެގް: %s' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'ލިޔުންތެރިޔާ: %s' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'އަހަރު: %s' ), get_the_date( _x( 'Y', 'yearly archives date format' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'މަސް: %s' ), get_the_date( _x( 'F Y', 'monthly archives date format' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'ދުވަސް: %s' ), get_the_date( _x( 'F j, Y', 'daily archives date format' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'އަރުޝީފް: %s' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @since 4.1.0
	 *
	 * @param string $title Archive title to be displayed.
	 */
	echo apply_filters( 'get_the_archive_title', $title );
}
if (!function_exists('the_post_thumbnail_caption'))
{
function the_post_thumbnail_caption() {
        echo '<span>'.get_the_post_thumbnail_caption().'</span>';
}

}


if (!function_exists('the_post_transliteration'))
{
	global $post;

	function the_post_transliteration($ID) {
		// var_dump(get_post_meta( $ID, 'post_transliteration', true ), $ID);
	    if($transliteration = get_post_meta( $ID, 'post_transliteration', true )){
	    	echo $transliteration;
	    }
	    else
	    	echo Thaana_Transliterator::transliterate(get_the_title($ID));
	}
}


			   

if (!function_exists('get_the_post_thumbnail_caption'))
{
function get_the_post_thumbnail_caption() {
	global $post;

	$thumbnail_id    = get_post_thumbnail_id($post->ID);
	$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

	if ($thumbnail_image && isset($thumbnail_image[0])) {
		return $thumbnail_image[0]->post_excerpt;
	}
}

}
function the_dhi_date()
{
    $dhi = [
        'ޖަނަވަރީ',
        'ފެބްރުއަރީ',
        'މާރޗް',
        'އެޕްރީލް',
        'މެއި',
        'ޖޫން',
        'ޖުލައި',
        'އޯގަސްޓް',
        'ސެޕްޓެމްބަރ',
        'އޮކްޓޯބަރ',
        'ނޮވެމްބަރ',
        'ޑިސެމްބަރ',
    ];
	global $post;
	\Carbon\Carbon::setLocale('dv');

	$carbon = \Carbon\Carbon::parse(get_the_time('Y-m-d H:m', $post->ID));
    echo sprintf("%s %s %s", $carbon->day, $dhi[$carbon->month-1], $carbon->year);
}

function dhi_date($date)
{
    $dhi = [
        'ޖަނަވަރީ',
        'ފެބްރުއަރީ',
        'މާރޗް',
        'އެޕްރީލް',
        'މެއި',
        'ޖޫން',
        'ޖުލައި',
        'އޯގަސްޓް',
        'ސެޕްޓެމްބަރ',
        'އޮކްޓޯބަރ',
        'ނޮވެމްބަރ',
        'ޑިސެމްބަރ',
    ];
	global $post;
	\Carbon\Carbon::setLocale('dv');

	$carbon = \Carbon\Carbon::parse(get_the_time('Y-m-d H:m', $date));
    echo sprintf("%s %s %s", $carbon->day, $dhi[$carbon->month-1], $carbon->year);
}
function get_human_date()
{
	global $post;
    \Carbon\Carbon::setLocale('dv');
    $carbon = \Carbon\Carbon::parse(get_the_time('Y-m-d H:m', $post->ID));
    return $carbon->diffForHumans();// $carbon->diffForHumans();
}

function the_human_date(){

    echo get_human_date();
}

function the_advertisment($term)
{
	$ad = Ishthihaaru::getRandomAdvertisement($term);
    echo sprintf("<a href='%s' target='_blank'><img class='advertisement' src='%s'></a>",$ad['link'],$ad['image'] );
}



add_filter('get_avatar', 'twbs_avatar_class');
function twbs_avatar_class($class) {
    $class = str_replace("class='avatar", "class='avatar img-circle media-object", $class);
    return $class;
}

add_filter('comment_reply_link', 'twbs_reply_link_class');
function twbs_reply_link_class($class){
    $class = str_replace("class='comment-reply-link", "class='comment-reply-link", $class);
    return $class;
}



function preprocess_comment_remove_url( $commentdata ) {
	// Always remove the URL from the comment author's comment
	unset( $commentdata['comment_author_url'] );

	// If the user is speaking in all caps, lowercase the comment
	if( $commentdata['comment_content'] == strtoupper( $commentdata['comment_content'] )) {
		$commentdata['comment_content'] = strtolower( $commentdata['comment_content'] );
	}
//	return false;
	return $commentdata;
}
add_filter( 'preprocess_comment' , 'preprocess_comment_remove_url' );





// Bootstrap pagination function

function wp_bs_pagination($pages = '', $range = 4)

{

	$showitems = ($range * 2) + 1;



	global $paged;

	if(empty($paged)) $paged = 1;



	if($pages == '')

	{

		global $wp_query;

		$pages = $wp_query->max_num_pages;

		if(!$pages)

		{

			$pages = 1;

		}

	}



	if(1 != $pages)

	{

		echo '<div class="text-center">';
		echo '<nav><ul class="pagination"><li class="disabled hidden-xs"><span>'.$pages.'<span aria-hidden="true"> '.__('Page', 'kulhudhufushi').'ގެ ތެރެއިން '.$paged.' '.'</span></span></li>';

		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' aria-label='First'>&laquo;"."</span></a></li>";

		if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Previous'>&lsaquo;</a></li>";



		for ($i=1; $i <= $pages; $i++)

		{

			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))

			{

				echo ($paged == $i)? "<li class=\"active\"><span>".$i." <span class=\"sr-only\">(current)</span></span>

    </li>":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>";

			}

		}



		if ($paged < $pages && $showitems < $pages) echo "<li><a href=\"".get_pagenum_link($paged + 1)."\"  aria-label='Next'>&rsaquo;</a></li>";

		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' aria-label='Last'>&raquo;</a></li>";

		echo "</ul></nav>";
		echo "</div>";
	}


}

add_action( 'wp_ajax_nopriv_remove_ad', 'remove_ad' );
add_action( 'wp_ajax_remove_ad', 'remove_ad' );

function remove_ad()
{
	$add = $_POST['ad_id'];
	$id = $_POST['id'];

	$meta_box_value = get_post_meta($id,'ad_'.$id,true);
    $link_string = get_post_meta($id,'ad_links_'.$id,true);

    $adImages = explode(';', $meta_box_value);
    $adLinks = explode(';', $link_string);



    $index = array_search($add, $adImages);

    array_splice($adImages, $index, 1);
    array_splice($adLinks, $index, 1);


    update_post_meta($id, 'ad_'.$id , implode(";", $adImages));
    update_post_meta($id, 'ad_links_'.$id , implode(";", $adLinks));
    return true;

}


add_action( 'wp_ajax_nopriv_like_comment', 'like_comment' );
add_action( 'wp_ajax_like_comment', 'like_comment' );

function like_comment() {
	if ( array_key_exists('HTTP_X_REQUESTED_WITH', $_SERVER) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
//		// I'm AJAX!
//
		$emotions = array( 'like'=> 'likes', 'dislike' => 'dislikes','love'=>'love' );
		$commentId = $_POST['comment_id'];
		$emotion = $_POST['emotion'];

		$likes = get_comment_meta($commentId, $emotions[$emotion] , true);
		$IP = get_comment_meta($commentId,'LastIP', true);
		$ID = get_comment_meta($commentId,'LastCommentID', true);


		if($IP != $_SERVER['REMOTE_ADDR'] && $ID != $commentId)
		{
	//		if(is_null($likes)) $likes = 0;
			update_comment_meta($commentId, $emotions[$emotion], $likes+1);
			update_comment_meta($commentId, 'LastIP', $_SERVER['REMOTE_ADDR']);

			echo json_encode(
				array(
					'status'=> true,
					'count' => $likes+1,
					'comment_id'=> $commentId,
					'emotion' => $emotion

				)
			);
		}
		else
			echo json_encode(array('status'=> false, 'message' => "You just made an impression"));
	}
	die();
}


function enable_extended_upload ( $mime_types =array() ) {
 
   // The MIME types listed here will be allowed in the media library.
   // You can add as many MIME types as you want.
   $mime_types['gz']  = 'application/x-gzip';
   $mime_types['zip']  = 'application/zip';
   $mime_types['rtf'] = 'application/rtf';
   $mime_types['ppt'] = 'application/mspowerpoint';
   $mime_types['ps'] = 'application/postscript';
   $mime_types['flv'] = 'video/x-flv';
 
   // If you want to forbid specific file types which are otherwise allowed,
   // specify them here.  You can add as many as possible.
   unset( $mime_types['exe'] );
   unset( $mime_types['bin'] );
 
   return $mime_types;
}
 
add_filter('upload_mimes', 'enable_extended_upload');

function comment_thanks($location, $comment)
{
	if( empty( $_POST['redirect_to'] ) )
    {
        
        if( get_option('permalink_structure') == '' )
        {
            // If your URL looks like "?p=8" then we need an ampersand
            $rty_parameter_syntax = '&';
        }
        else
        {
            $rty_parameter_syntax = '?';
        }
        
        // Append a "thank you" parameter so the post or page template can display the appropriate message
        // Alternatively, you can put a specific page to redirect to
        // Or, you can check the value of the other properties of the $comment object to do something else
        // The $comment object contains: comment_ID, comment_post_ID, comment_author, comment_author_email, 
        // comment_author_url, comment_author_IP, comment_date, comment_date_gmt, 
        // comment_content, comment_karma, comment_approved (1 for approved; 0 for moderation queue; spam for spam), 
        // comment_agent, comment_type, comment_parent, user_id

        $location = get_permalink($comment->comment_post_ID) . $rty_parameter_syntax . 'thankyou';

        return $location;
    }
	return $location;
}
add_action('comment_post_redirect', 'comment_thanks', 15, 2);


function get_subtitle_count($postID)
{
    return get_post_meta($postID,'subtitle_count',true);
}

function get_author_by_id($user)
{
	if($user = get_user_by('ID', $user))
	{
		return "$user->first_name $user->last_name";
	}
    return '';
}

function get_block_count($postID)
{
    return get_post_meta($postID,'block_count',true);
}