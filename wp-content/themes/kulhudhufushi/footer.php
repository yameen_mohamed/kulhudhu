
<br>
</div>
<div class="push"></div>
</div>
<footer dir="ltr">
    <style>
        footer .media {
            margin: -20px;
            color: #fff !important;
        }
        footer .media .media-body a {
            color: #fff;
            padding-right: 25px !important;
        }
        h4.footer-heading {
            display: inline-block;
            border-bottom: 1px solid #ccc;
            padding-bottom: 10px;
        }
        div.foot {
            position: relative;
            display: block;
            clear: both;
            padding: 15px;
        }
        div.foot:after {
            width: calc(100% + 40px);
            height: 1px;
            content: ' ';
            background: #4FBB6F;
            position: absolute;
            margin-right: -40px;
        }
        .copyright {
            height: 8vh;
            line-height: 8vh;
            padding-right: 40px;
        }

        div.dark-inset{
            background: #636363;
            box-shadow: inset 0 0 30px 0 #131313;
        }
        .nav li{
            /*display: inline-block;*/
        }
        .nav li a{
            color: #fff;;

        }

        .nav > li.footer-nav-item > a:hover, .nav > li > a:focus {
            text-decoration: none;
            background-color: transparent;
            color: #006600;

        }
        li.footer-nav-item {
            display: inline-block;
        }
    </style>
    <div class="jumbotron top-bar" style="direction: rtl; font-family: arial;color: #fff">
        <div class="container">


            <div class="row">
                <img class="pull-left" style="max-width: 220px" src="<?php echo get_template_directory_uri() ?>/images/k-online.png">


                <div class="col-md-4">
                    <?php
                        $menus = wp_get_nav_menu_items('Footer Menu');
                    ?>
                    <ul class="nav">
                        <?php
                            foreach($menus as $menu): ?>
                            <li class="footer-nav-item"><a href="<?php echo $menu->url; ?>" class="waheed"><?php echo $menu->title; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/app.js"></script>

<script type="text/javascript">
    function copyTextToClipboard(text) {
        var textArea = document.createElement("textarea");

        //
        // *** This styling is an extra step which is likely not required. ***
        //
        // Why is it here? To ensure:
        // 1. the element is able to have focus and selection.
        // 2. if element was to flash render it has minimal visual impact.
        // 3. less flakyness with selection and copying which **might** occur if
        //    the textarea element is not visible.
        //
        // The likelihood is the element won't even render, not even a flash,
        // so some of these are just precautions. However in IE the element
        // is visible whilst the popup box asking the user for permission for
        // the web page to copy to the clipboard.
        //

        // Place in top-left corner of screen regardless of scroll position.
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textArea.style.width = '2em';
        textArea.style.height = '2em';

        // We don't need padding, reducing the size if it does flash render.
        textArea.style.padding = 0;

        // Clean up any borders.
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';

        // Avoid flash of white box if rendered for any reason.
        textArea.style.background = 'transparent';


        textArea.value = text;

        document.body.appendChild(textArea);

        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
        }

        document.body.removeChild(textArea);
    }

    $(function(){
        $.material.init();

        $('#commentModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget);
          var parent = button.data('parent');
          var modal = $(this);



          modal.find('#comment_parent').val(parent);

            console.log('CommentID:' + parent, button)

        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        $('.carousel').carousel();


        $('.thaana').on('keypress', function(e){
            if(e.which != 13)
                return Keyboard.thaana(e);
        })
        var navbar = $('#navbar-container');
        var $top = $('div.navbar').offset().top;
        $(window).scroll(function(){
            if($(window).scrollTop() <= $top){
                window.navbar_fixed = false;
                navbar.removeClass('fixed spread');
            } else {
                if(!window.navbar_fixed){
                    console.log('this:' , 'yes passed');
                    navbar.addClass('fixed');
                    setTimeout(function(){
                        navbar.addClass('spread')
                    }, 0);
                }
                window.navbar_fixed = true;

            }
        });

        $('.btn-emtion').on('click', function()
        {
            var self = this;
            $.ajax({
                type: 'post',
                data: {
                    action: 'like_comment',
                    emotion: $(self).data('emotion'),
                    comment_id: $(self).data('commentid'),
                },
                url: '/wp-admin/admin-ajax.php',
                success: function(data)
                {
                    $data = JSON.parse(data);

                    console.log($data);

                    if($data.status)
                    {
                        console.log('#like_comment'+ $data.comment_id);
                        $('#' + $data.emotion + '_comment'+ $data.comment_id).html($data.count);
                    }
                    else
                    {
                        alert($data.message);
                    }
                }
            }) ;
        });
        $('.share').affix({
          offset: {
            top: function () {
              return $('.entry-content').offset().top-50 ;
            },
            bottom: function () {
              return $('.wrapper').outerHeight() -  ($('.entry-content').offset().top+$('.entry-content').height() - 100);
            }
          }
        });

        $('.copy-href').on('click', function()
        {
            copyTextToClipboard($(this).attr('href'));
            alert('Link copied');
            return false;
        });
    });




</script>
<div id="fb-root"></div>

<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
      if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.'); 
        return;
      }

      if (Notification.permission !== "granted")
        Notification.requestPermission();
    });

    function notifyMe() {
      if (Notification.permission !== "granted")
        Notification.requestPermission();
      else {
        var notification = new Notification('Notification title', {
          icon: 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
          body: "Hey there! You've been notified!",
        });

        notification.onclick = function () {
          window.open("https://stackoverflow.com/a/13328397/1269037");      
        };

      }

    }
</script>
<script src="https://js.pusher.com/4.0/pusher.min.js"></script>
  <script>

    var pusher = new Pusher('89335e29a0cdde9d5bea', {
      cluster: 'ap2',
      encrypted: true
    });

    var channel = pusher.subscribe('updates');
    channel.bind('notify', function(data) {
        var notification = new Notification(data.name, {
          icon: data.icon,
          body: data.message,
        });

        notification.onclick = function () {
          window.open(data.url);      
        };
    });


  </script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

</html>
