<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

$image_title = get_the_post_thumbnail_caption();
?>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@site_username">
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:description" content="<?php the_excerpt( ); ?>">
<meta name="twitter:creator" content="@mnu_mv">
<meta name="twitter:image:src" content="<?php the_post_thumbnail_url(); ?>">
<meta name="twitter:domain" content="beta.kulhudhuffushi.com">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <p class="mute-date"><?php echo get_human_date().' - '; the_dhi_date() ?></p>


    <div class="entry-content faseyha">
        <?php
        $meta_box_value = get_post_meta($post->ID,'embed_code',true);
        if($meta_box_value) { echo html_entity_decode($post->embed_code); }
        ?>
    </div><!-- .entry-content -->
    <header class="entry-header waheed">
        <?php the_title( '<h1 class="waheed entry-title">', '</h1>' ); ?>
    </header><!-- .entry-header -->
    <footer class="entry-footer">

        <div class="fb-share-button" data-href="<?php the_permalink() ?>" data-layout="button"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-via="mnu_mv">Tweet</a>
        <?php
        //		kulhudhufushi_entry_meta();
        ?>
        <?php
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'kulhudhufushi' ),
                get_the_title()
            ),
            '<span class="edit-link">',
            '</span>'
        );
        ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
