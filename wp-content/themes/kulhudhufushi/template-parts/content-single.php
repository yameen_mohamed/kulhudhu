<style type="text/css">
	@media (max-width: 768px)
    {
        .container.fixed-stage {
            margin-top: 0 !important;
        }
    }

	.block-content {
    	margin-right: 10px;
		float: left;
		display: inline-block;
	}

	.block-wrap {
		display: flex;
	}

	.block-wrap .time {
		font-size: small;
	}

	.block span {
		float: right;
		padding: 0px;
		margin-left: 10px;
	}

	.block {
		display: block;
		clear: both;
	}
	.author {
		margin: 0px 0px 10px 10px;
		border-bottom: 1px solid #ccc;
		color: gray;
		font-size: 12px;
	}
</style>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	$date = get_post_meta($post->ID,'breaking_expiry_date',true);
	if( strtotime($date) >= strtotime(date('m/d/Y h:i:s a', time()))) :
	?>
	<span class="waheed break-tag">ކުއްލި ޚަބަރު</span>
	<?php endif; ?>
	<header class="entry-header ">
		<a href="<?php the_permalink(); ?>"><?php the_title( '<h1 class="waheed entry-title">', '</h1>' ); ?></a>
	</header><!-- .entry-header -->

	<ul>
	<?php 
		for ($i=1; $i <= get_subtitle_count($post->ID); $i++):
	?>
			<li class='waheed'> <?= get_post_meta($post->ID, 'title-'.$i, true) ?> </li>
	<?php 
		endfor;
	?>
	</ul>

	<p class="faseyha mute-date no-bottom-border"><?php the_author(); ?> - <?php echo get_human_date().' - '; the_dhi_date() ?></p>
	<hr style="margin-top: 0">
		<img class="" width="100%" src="<?php the_post_thumbnail_url() ?>">
		<div class="extra">
			<?php global $image_title; if(!empty($image_title)): ?>
			<span class="quote " style="font-size: 15px; margin-bottom: 0;"><?= $image_title ?></span>
			<?php endif; ?>
				<?php echo get_the_category_list() ?>
			<?php echo get_the_tag_list() ?>


		</div>
	<hr style="margin-top: -10px">

	<div class="entry-content faseyha">
		<div class="row">
			<div class="col-md-3">
				<ul class="share">
            		<li><a href="https://www.facebook.com/sharer/sharer.php?kid_directed_site=0&sdk=joey&u=<?php the_permalink() ?>&display=popup&ref=plugin&src=share_button" target="_blank">
            		<img src="<?php echo get_template_directory_uri() ?>/images/facebook-icon.png"></a></li> 
            		<li><a href="https://twitter.com/intent/tweet?text=<?php the_title() ?>&amp;url=<?php the_permalink() ?>" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/twitter-icon.png"></a></li>
            
            		<li><a href="https://plus.google.com/share?url=<?php the_permalink() ?>&amp;hl=en-GB&amp;wwc=1" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/images/google-plus-icon.png"></a></li> 
            		<li><a id='viberButton' href="#"><img src="<?php echo get_template_directory_uri() ?>/images/viber-icon.png"></a></li> 
          </ul>

			</div>
			<div class="col-md-9">
			<?php 
				for ($i=get_block_count($post->ID); $i > 0 ; $i--):
			?>
				<div class="block">
					<div class='waheed block-wrap'>
						<span class='time'>
							<?= date('H:i',strtotime(get_post_meta($post->ID, 'block-'.$i.'-date', true))) ?>	
						</span>
						<div class="block-content faseyha">
							<?= get_post_meta($post->ID, 'block-'.$i, true) ?> 
							<div class="clearfix"></div>
							<div class="author">
								<b class='waheed'><?= get_author_by_id(get_post_meta($post->ID, 'block-'.$i.'-user', true)) ?></b>	
							</div>
						</div>
					</div>

				</div>
			<?php

				endfor;
			?>

				<?php
					the_content();

		//			wp_link_pages( array(
		//				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'kulhudhufushi' ) . '</span>',
		//				'after'       => '</div>',
		//				'link_before' => '<span>',
		//				'link_after'  => '</span>',
		//				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'kulhudhufushi' ) . ' </span>%',
		//				'separator'   => '<span class="screen-reader-text">, </span>',
		//			) );

					if ( '' !== get_the_author_meta( 'description' ) ) {
						get_template_part( 'template-parts/biography' );
					}
				?>
			</div>
		</div>	
	</div><!-- .entry-content -->

	<div class="side-advertisement">
	<?php the_advertisment('BeforeComments') ?>
</div>

	<footer class="entry-footer">

		<!-- <div class="fb-share-button" data-href="<?php the_permalink() ?>" data-layout="button"></div> -->
		<!-- <a href="https://twitter.com/share" class="twitter-share-button" data-via="mnu_mv">Tweet</a> -->

	    <script>
	    var buttonID = "viberButton";
	    var text = "Check this out: ";
	    document.getElementById(buttonID).setAttribute('href', "viber://forward?text=" + encodeURIComponent(text + " " + window.location.href));



	    </script>
		<?php
//		kulhudhufushi_entry_meta();
		?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'kulhudhufushi' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
