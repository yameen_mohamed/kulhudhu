<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
            <span class="sticky-post"><?php _e( 'Featured', 'kulhudhufushi' ); ?></span>
        <?php endif; ?>

        <?php the_title( sprintf( '<h3 class="waheed entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
    </header><!-- .entry-header -->
    <img width="100%" src="<?php the_post_thumbnail_url() ?>" alt="">
    <span class="faseyha">
        <?php kulhudhufushi_excerpt(); ?>
    </span>


    <div class="entry-content">
        <?php
        /* translators: %s: Name of current post */
        //			the_content( sprintf(
        //				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'kulhudhufushi' ),
        //				get_the_title()
        //			) );

        //			wp_link_pages( array(
        //				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'kulhudhufushi' ) . '</span>',
        //				'after'       => '</div>',
        //				'link_before' => '<span>',
        //				'link_after'  => '</span>',
        //				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'kulhudhufushi' ) . ' </span>%',
        //				'separator'   => '<span class="screen-reader-text">, </span>',
        //			) );
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
<!--        --><?php //kulhudhufushi_entry_meta(); ?>
        <?php
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'kulhudhufushi' ),
                get_the_title()
            ),
            '<span class="edit-link">',
            '</span>'
        );
        ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
