<style type="text/css">
    @media screen and (max-width: 492px)
    {
        #slider {
            display: none;
        }
        #slider_alt
        {
            display: block !important;
        }
    }
</style>
<?php
$postslalalalal = new WP_Query(array(
    'post_type'  => 'post',
    'order'=> 'DESC',
    'orderby' => 'date',
//    'offset'    => $offset,
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'=>'show_on_carousel',
            'value'=>'true'
        )
    ),

));

?>
<div id="slider_alt" class="hide carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <?php $index=0; while($postslalalalal->have_posts()): $postslalalalal->the_post(); ?>
        <div class="item active">
            <?php
            $date = get_post_meta($post->ID,'breaking_expiry_date',true);
            if( strtotime($date) >= strtotime(date('m/d/Y h:i:s a', time()))) :
                ?>
            <span class="waheed breaking">ކުއްލި ޚަބަރު</span>

            <?php endif ?>
            <?php  the_post_thumbnail(array(560, 354)); ?>
            <div class="carousel-caption">
                <a href="<?php the_permalink() ?>">
                    <h4 class="thaana waheed"><?php the_title(); ?></h4>
                </a>

            </div>
        </div>
        <?php $index++; endwhile; wp_reset_query(); ?>
    </div>
</div>
<div id="slider" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <?php $index=0; while($postslalalalal->have_posts()): $postslalalalal->the_post(); ?>
        <div class="item <?php if($index==0) echo 'active'; ?>">
            <?php
            $date = get_post_meta($post->ID,'breaking_expiry_date',true);
            if( strtotime($date) >= strtotime(date('m/d/Y h:i:s a', time()))) :
                ?>
            <span class="waheed breaking">ކުއްލި ޚަބަރު</span>

            <?php endif ?>
            <?php  the_post_thumbnail(array(560, 354)); ?>
            <div class="carousel-caption">
                <a href="<?php the_permalink() ?>">
                    <h3 class="thaana "><?php the_title(); ?></h3>
                </a>
                <p><?php the_excerpt(); ?></p>

            </div>
        </div>
        <?php $index++; endwhile; wp_reset_query(); ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#slider" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>