<?php


$carousel_news = new WP_Query(array(
    'post_type'  => 'post',
    'posts_per_page' => 4,
    'order'=> 'DESC',
    'orderby' => 'date',
    'offset'    => $offset,
    'post__not_in' => $post_ids,
    'post_status' => 'publish',
//    'meta_query' => array(
//        array(
//            'key' => '_thumbnail_id',
//            'compare' => 'EXISTS'
//        ),
//    )
    )
);

$offset+= 4;
?>

<div class="list-group top-news-headline">
    <?php $index=0; while($carousel_news->have_posts()): $carousel_news->the_post(); ?>
    <div class="list-group-item">
        <div class="row-action-primary <?php  if($index > 0) echo 'medium-caps' ?>">
            <i class="mdi-file-er thumbnail-holder">
                <?php if(has_post_thumbnail()): ?>

                    <img class="thumbnail" src="<?php echo get_the_post_thumbnail_url()  ?>">
                <?php endif; ?>
            </i>
        </div>
        <div class="row-content <?php  if($index > 0) echo 'medium-caps' ?>">
            <a href="<?php the_permalink() ?>">
                <h4 class="list-group-item-heading waheed"><?php the_title(); ?></h4>
            </a>
        </div>
    </div>
    <div class="list-group-separator"></div>
    <?php $index++; endwhile;wp_reset_query(); ?>
</div>
