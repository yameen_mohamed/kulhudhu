<style type="text/css">
    .navbar{
        transition: all .3s ease-in-out;
    }

    div#navbar-container {
        width: 100%;
        left: 0;
        background: #F44336;
    }

    #navbar-container.fixed {
        position: fixed;
        width: 98%;
        padding: 0;
        margin: 0;
        top: 0;
        left: 1%;
        z-index: 1;
        transition: all .3s ease-in;
    }

    div#navbar-container.spread{
        width: 100%;
        left: 0;
    }

    img.brand-image
    {
        width: 0;
        display: inline-block;
        transition: width 0.3s ease-in-out;
    }
    #navbar-container.fixed img.brand-image
    {
        width: 50px !important;
        box-shadow: -1px 0 6px #3DB05F;
    }
    .carousel-control
    {
        background: none !important;
        color: #100F0F;

    }
    .carousel-control.left
    {
        left: -30px;
    }
    .carousel-control.right
    {
        /*bottom: -50px;*/
        right: -20px;
    }

    .navbar-material-red.navbar .dropdown-menu li > a:hover, .navbar-material-red.navbar .dropdown-menu li > a:focus
    {
        color: #fff;
    }


</style>
<div id="navbar-container" class="shadow">
    <div class="container navbar navbar-material-red">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="padding: 0" href="javascript:void(0)">
                <img class="brand-image" width="200px" src="<?php echo get_template_directory_uri() ?>/images/k-logo.png">
            </a>
        </div>
        <div class="navbar-collapse collapse navbar-responsive-collapse">
            <?php wp_nav_menu( array(
                'theme_location'    => 'primary',
                'menu_class'        => 'nav navbar-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker()
            ) ); ?>
            <form class="navbar-form hidden-sm navbar-left" method="get" action="/">
                <input type="text" id="searchtxt" name="s" value="<?php echo isset($_GET['s']) ? $_GET['s'] : ''; ?>" class="thaana form-control col-lg-8" placeholder=" ހޯދާ ...">
            </form>
        </div>
    </div>
</div>
