<?php

$cat_column = get_category_by_slug('internation_sports');

$column_args = array (
    'category_name'          => $cat_column->category_nicename,
    'nopaging'               => false,
    'posts_per_page'         => '5',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'post__not_in' => $post_ids,
);
$home_column = new WP_Query($column_args);

?>
<div class="panel panel-material-red">
    <div class="panel-heading waheed">
        <a href="<?php echo get_term_link('internation_sports','category') ?>">
        ދުނިޔޭގެ ކުޅިވަރު
        </a>
    </div>
    <div class="panel-body">
        <div class="list-group">
            <?php $index=0; while($home_column->have_posts()): $home_column->the_post(); ?>
            <div class="list-group-item">
                <?php if($index == 0 && has_post_thumbnail()): ?>
                <div class="list-group-cover">
                    <img src="<?php echo get_the_post_thumbnail_url() ?>"  alt="...">
                </div>
                <?php else: ?>
                        <div class="row-action-primary">
                            <?php if(has_post_thumbnail()): ?>
                            <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="">
                            <?php else: ?>
                            <i class="mdi-file-folder"></i>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                <div class="row-content   <?php if($index > 0) echo 'small-caps' ?>">
                    <div class="action-secondary"><i class="mdi-material-info"></i></div>
                    <a href="<?php the_permalink() ?>">
                        <h4 class="list-group-item-heading"><?php the_title() ?></h4>
                    </a>

<!--                    <p class="list-group-item-text">--><?php //the_excerpt() ?><!--</p>-->
                </div>
            </div>
            <div class="list-group-separator"></div>
            <?php $index++; endwhile; ?>
        </div>
    </div>
</div>
