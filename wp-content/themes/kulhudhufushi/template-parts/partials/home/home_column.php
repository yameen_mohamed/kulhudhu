<?php

$cat_column = get_category_by_slug('column');

$column_args = array (
    'category_name'          => $cat_column->category_nicename,
    'nopaging'               => false,
    'posts_per_page'         => '5',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'post__not_in' => $post_ids,
);
$home_column = new WP_Query($column_args);

?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-material-red">
            <div class="panel-heading waheed"><a href="<?php echo get_term_link('column','category') ?>">ކޮލަމް</a></div>
            <div class="panel-body">
                <div class="list-group">
                <?php $index = 0; while($home_column->have_posts()) : $home_column->the_post(); ?>
                    <div class="list-group-item">


                        <?php if($index == 0 && has_post_thumbnail()): ?>
                        <div class="list-group-cover">
                            <img src="<?php the_post_thumbnail_url(); ?>" alt="...">
                        </div>
                        <?php else: ?>
                        <div class="row-action-primary">
                            <?php if(has_post_thumbnail()): ?>
                            <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="">
                            <?php else: ?>
                            <i class="mdi-file-folder"></i>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>

                        <div class="row-content  <?php if($index > 0) echo 'small-caps' ?>">
                            <div class="action-secondary"><i class="mdi-material-info"></i></div>
                            <a href="<?php the_permalink() ?>"><h4 class="list-group-item-heading"><?php the_title(); ?></h4></a>
                        </div>

                    </div>
                    <div class="list-group-separator"></div>
                    <?php $index++; endwhile; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <!-- <img class="stretch" src="/images/idol.png" alt=""> -->
<!--        @include('partials.advertisements.home_first_row_left')-->
    </div>
</div>
