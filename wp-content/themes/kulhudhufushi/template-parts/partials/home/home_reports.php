<?php

$cat_column = get_category_by_slug('reports');

$column_args = array (
    'category_name'          => $cat_column->category_nicename,
    'nopaging'               => false,
    'posts_per_page'         => '7',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'post__not_in' => $post_ids,
);
$home_column = new WP_Query($column_args);

?>
<div class="panel panel-material-red">
    <div class="panel-heading waheed">
        <a href="<?php echo get_term_link('reports','category') ?>">
        ރިޕޯޓް
        </a>
    </div>
    <div class="panel-body">
        <div class="list-group">
            <?php $index=0; while($home_column->have_posts()): $home_column->the_post(); ?>
                <?php if($index==0 && has_post_thumbnail()): ?>
                    <div class="list-group-item">

                        <div class="list-group-cover">
                            <img src="<?php echo get_the_post_thumbnail_url() ?>"  alt="...">
                        </div>
                        <div class="row-content   <?php if($index > 0) echo 'small-caps' ?>">
                            <div class="action-secondary"><i class="mdi-material-info"></i></div>
                            <a href="<?php the_permalink() ?>">
                                <h4 class="list-group-item-heading"><?php the_title() ?></h4>
                            </a>
                        </div>
                    </div>
                    <div class="list-group-separator"></div>

                <?php else: ?>
                        

                    <?php if($index % 3 == 0): ?>
                        <div class="row">
                    <?php endif; ?>
                    <div class="col-md-4">
                        <div class="list-group-item">
                            
                            
                            <div class="row-co   <?php if($index > 0) echo 'small-caps' ?>">
                                <?php if(has_post_thumbnail()): ?>
                                <img class="thumnail" src="<?php the_post_thumbnail_url('medium'); ?>" alt="">
                                <?php else: ?>
                                <i class="mdi-file-folder"></i>
                                <?php endif; ?>
                                <!-- <div class="action-secondary"><i class="mdi-material-info"></i></div> -->
                                <a href="<?php the_permalink() ?>">
                                    <h4 class="list-group-item-heading"><?php the_title() ?></h4>
                                    <br>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php if($index > 0 && $index % 3 == 0): ?>
                        </div>
                    <?php endif; ?>



                <?php endif; ?>
                    
                
                <?php $index++; endwhile; ?>
        </div>
    </div>
</div>
