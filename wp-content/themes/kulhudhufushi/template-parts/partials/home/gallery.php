<div class="panel panel-material-red">
    <div class="panel-heading waheed"><a href="<?php echo get_post_type_archive_link('galleries') ?>">
    ފޮޓޯ ގެލަރީ
    </a></div>
    <div class="panel-body">
        <div class="">

            <?php

            $cat_galleries = get_category_by_slug('galleries');

            $galleries_args = array(
                'post_type' => 'galleries',
                'posts_per_page' => '3',
                'order' => 'DESC',
                'orderby' => 'id',
            );
            $home_galleries = new WP_Query($galleries_args);

            if ($home_galleries->have_posts()):
                while ($home_galleries->have_posts()): $home_galleries->the_post();
                    ?>
                         <?php
                            $image_count = get_post_meta($post->ID,'image_count',true);
                            if($image_count > 0):
                        ?>
                                <div class="media"> 
                                    <div class="media-left"> 
                                        <a href="<?php the_permalink();  ?>"> 
                                            <img class="media-object" data-src="<?php echo get_post_meta($post->ID,'image_1', true) ?>"  data-holder-rendered="true" style="width: 64px; height: 64px;" src="<?php echo get_post_meta($post->ID,'image_1', true) ?>"> 
                                        </a> 
                                    </div> 
                                    <div class="media-body"> 
                                        <a href="<?php the_permalink();  ?>"><?php the_title(); ?></a> 
                                    </div> 
                                </div>

                        <?php
                        endif; ?>

                <?php endwhile;endif; ?>
        </div>

    </div>
</div>





