<div class="panel panel-material-red">
    <div class="panel-heading waheed"><a href="<?php echo get_post_type_archive_link('videos') ?>">އެންމެ ފަހުގެ ވީޑިއޯތައް</a></div>
    <div class="panel-body">
        <div class="row">

            <?php

            $cat_videos = get_category_by_slug('videos');

            $videos_args = array(
                'post_type' => 'videos',
                'posts_per_page' => '2',
                'order' => 'DESC',
                'orderby' => 'id',
            );
            $home_videos = new WP_Query($videos_args);

            if ($home_videos->have_posts()):
                while ($home_videos->have_posts()): $home_videos->the_post();
                    ?>

                    <div class="col-md-6">
                        <div class="video">
                            <?php echo html_entity_decode(get_post_meta(get_the_ID(), 'embed_code', true)); ?>
                        </div>
                        <?php if(!empty(get_the_title())): ?>
                        <div>
                            <span class="quote"><?php the_title() ?></span>
                        </div>
                        <?php endif; ?>
                    </div>

                <?php endwhile;endif; ?>
        </div>

    </div>
</div>





