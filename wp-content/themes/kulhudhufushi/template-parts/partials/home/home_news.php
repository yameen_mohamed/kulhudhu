<?php

$cat_news = get_category_by_slug('news');

$args = array (
    'category_name'          => $cat_news->category_nicename,
    'nopaging'               => false,
    'posts_per_page'         => '6',
    'order'                  => 'DESC',
    'orderby'                => 'date',
    'post__not_in' => $post_ids,
);


// $news_part2 = array (
//     'category_slug'          => 'news',
//     'nopaging'               => false,
//     'posts_per_page'         => '5',
//     'order'                  => 'DESC',
//     'orderby'                => 'date',
//     'offset'                => 6,
//     'post__not_in' => $post_ids,
// );


$news_one = array();
$news_two = array();

$home_news = new WP_Query( $args );
$index = 0;
while ($home_news->have_posts()){
    $home_news->the_post();
    // if($index++ % 2)
        // $news_two[] = $post;
    // else
        $news_one[] = $post;

} 


// var_dump($news_two);
// die();


?>

<div class="panel panel-material-red">
    <div class="panel-heading waheed"><a href="<?php echo get_term_link('news','category') ?>"> ފަހުގެ ޚަބަރު</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12 latest-news list-group">
                <div class="col-md-12">
                    <?php $index = 0;
                    foreach ($news_one as $post): ?>
                        <div class="list-group-item">
                            <?php if ($index == 0): ?>
                                <div class="list-group-cover">
                                    <img width="100%" src="<?php the_post_thumbnail_url() ?>" alt="image">
                                </div>
                            <?php else: ?>
                                <div class="row-action-primary">
                                    <?php if (has_post_thumbnail($post->ID)): ?>
                                        <img src="<?php the_post_thumbnail_url('thumbnail') ?>" alt="">
                                    <?php else: ?>
                                        <i class="mdi-file-folder"></i>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <div class="row-content @if($index > 0) small-caps @endif">
                                <div class="action-secondary"><i class="mdi-material-info"></i></div>
                                <a href="<?php the_permalink() ?>">
                                    <h4 class="list-group-item-heading"><?php echo $post->post_title; ?></h4>
                                </a>
<!--                                --><?php //if ($index == 0): ?>
<!--                                    <p class="list-group-item-text">--><?php //echo $post->post_excerpt; ?><!--</p>-->
<!--                                --><?php //endif; ?>
                            </div>
                        </div>

                        <div class="list-group-separator"></div>
                        <?php $index++;endforeach; ?>
                </div>
            </div>
            
        </div>

    </div>
</div>
