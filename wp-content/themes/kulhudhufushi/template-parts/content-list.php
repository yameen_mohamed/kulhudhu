<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 3/19/16
 * Time: 12:22 AM
 */?>


<div class="media">
    <div class="media-left">
        <a href="#">
            <img class="media-object" width="100" src="<?php echo has_post_thumbnail() ? the_post_thumbnail_url() : get_template_directory_uri().'/images/k-logo.png' ?>" alt="Loading">
        </a>
    </div>
    <div class="media-body">
        <a href="<?php the_permalink()?>">
            <h4 class="waheed media-heading"><?php echo $post->post_title; ?></h4>
        </a>
        <span class="date"><?php echo get_human_date(); ?></span>
    </div>
</div>
