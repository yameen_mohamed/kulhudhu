<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

$image_title = get_the_post_thumbnail_caption();
?>
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@site_username">
<meta name="twitter:title" content="<?php the_title(); ?>">
<meta name="twitter:description" content="<?php the_excerpt( ); ?>">
<meta name="twitter:creator" content="@mnu_mv">
<meta name="twitter:image:src" content="<?php the_post_thumbnail_url(); ?>">
<meta name="twitter:domain" content="beta.kulhudhuffushi.com">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header waheed">
       <a href="<?php the_permalink() ?>"> <?php the_title( '<h1 class="waheed entry-title">', '</h1>' ); ?></a>
    </header><!-- .entry-header -->
    <p class="mute-date"><?php echo get_human_date().' - '; the_dhi_date() ?></p>


    <div class="entry-content faseyha">
        <div id="slider" dir="rtl">
            <?php
            $image_count = get_post_meta($post->ID,'image_count',true);
            if($image_count > 0):
            for($i=0; $i < $image_count;$i++):
            ?>
                <?php if($i==0): ?>
                    <img style="max-width: 100%" class="" src='<?php echo get_post_meta($post->ID,'image_'.($i+1), true) ?>' />
                <?php else: ?>
                    <img style="max-width: 20%" class="" src='<?php echo get_post_meta($post->ID,'image_'.($i+1), true) ?>' />

                <?php endif; ?>

            <?php
            endfor;
            endif; ?>
        </div>

        
    </div><!-- .entry-content -->
    
    <footer class="entry-footer">

        <div class="fb-share-button" data-href="<?php the_permalink() ?>" data-layout="button"></div>
        <a href="https://twitter.com/share" class="twitter-share-button" data-via="mnu_mv">Tweet</a>
        <?php
        //		kulhudhufushi_entry_meta();
        ?>
        <?php
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'kulhudhufushi' ),
                get_the_title()
            ),
            '<span class="edit-link">',
            '</span>'
        );
        ?>
    </footer><!-- .entry-footer -->
</article><!-- #post-## -->
