<!DOCTYPE html dir=rtl>
<html>
<head>
    <title>Kulhudhuffushi Online - <?php echo $post->post_title; ?></title>
    <link rel="shortcut icon" type="image/gif" href="//kulhudhuffushi.com/wp-content/themes/kulhudhufushi/images/fav-ico.gif"/>

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/app.css">
    <style type="text/css">
        html, body {
            direction: ltr !important;
            background: #000;
            color: #fff;
        }
        .slider {
            margin: 0 auto;
            height: calc(100% - 100px);
        }
        #slider .slick-slide
        {
            position: relative;
        }

        #slider .slick-slide figcaption {
            position: absolute;
            bottom: 0;
            width: 100%;
            padding: 20px;
            background: rgba(204, 204, 204, 0.2);
        }


        #slider .slick-slide img,#slider  .slick-slide.slick-current.slick-active img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .waheed, .faruma
        {
            direction: rtl !important;
        }
    </style>
    <style type="text/css">
        
        .slider-nav-thumbnails img {
            padding: 5px 5px 5px 0px;
            object-fit: cover;

        }
        .slider-nav-thumbnails .slick-slide
        {
            position: relative;
            display: inline-block;
            background-color: #000;
        }
        .slider-nav-thumbnails .slick-slide.slick-current {
            /*border: 1px solid red;*/
            /*transform: scale(1.1);*/
            padding: 5px;
        }

        .slider-nav-thumbnails {
            height: 100px;
            overflow: hidden;
        }

        .slick-prev{
            left: 25px;
            z-index: 1111;
        }

        .slick-next {
            right: 25px;
            z-index: 1111;
        }
        .nav-gallery {
            position: relative;
        }
        .nav-gallery .slick-next {
            right: 5px;
        }
        .nav-gallery .slick-prev {
            left: 5px;
        }
        img.overlay {
            position: fixed;
            top: 10px;
            left: 10px;
            z-index: 10000;
            height: 70px;
            background: rgba(0, 0, 0, 0.23);
            padding: 10px;
        }

        .fb-icon
        {
            background-image: url(https://www.facebook.com/rsrc.php/v3/y6/r/paQxSiG0mYy.png);
            background-repeat: no-repeat;
            background-size: auto;
            background-position: 0 -13px;
            display: block;
            height: 24px;
            outline: none;
            overflow: hidden;
            text-indent: -999px;
            white-space: nowrap;
            width: 24px;
            margin: 10px;
        }

    </style>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Kulhudhufushi.com">
    <meta name="twitter:title" content="<?php the_title(); ?>">
    <meta name="twitter:description" content="<?php the_excerpt( ); ?>">
    <meta name="twitter:creator" content="@kulhudhuffushi">
    <meta name="twitter:image:src" content="<?php the_post_thumbnail_url(); ?>">
    <meta name="twitter:domain" content="www.kulhudhuffushi.com">

    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Kulhudhufushi Online">
    <meta property="og:title" content="<?php the_title(); ?>">
    <meta property="og:description" content="<?php the_excerpt( ); ?>">
    <meta property="og:url" content="<?php the_permalink(); ?>">
    <meta property="og:locale" content="en">
    <meta property="og:image" content="<?php the_post_thumbnail_url(); ?>">
    <meta property="og:image:width" content="968">
    <meta property="og:image:height" content="504">
</head>
<body >
    <img class="overlay" src="http://kulhudhu.dev/wp-content/themes/kulhudhufushi/images/k-online.png">
    <div class="fluid-container">
        <div class="rw">
            <div class="col-md-2">
                <div class="row">
                    <h3 class="waheed">
                        <?php the_title() ?>
                    </h3>
                    <div class="faruma">
                        <?php the_excerpt() ?>
                    </div>
                    <hr>
                    <div class="nav-gallery">
                        <?php next_post_link( '%link', '<span class="inline slick-next slick-arrow"></span>' ); ?>
                        <?php previous_post_link( '%link', '<span class="inline slick-prev slick-arrow"></span>' ); ?>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>" target="_blank"><span class="fb-icon"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div id='slider' class="slider">

                        <?php

                            $nav = '';
                            $image_indices = get_post_meta($post->ID,'image_indices', true);
                            if(!is_null($image_indices)):
                                foreach(explode(',', $image_indices) as $item):
                                $image = get_post_meta($post->ID,'image_'.($item), true);
                                $caption = get_post_meta($post->ID,'image_desc_'.($item), true);
                                    if($image):
                                        $nav.="<img src='$image'>";

                            ?>
                                <figure>
                                    <img src="<?php echo $image; ?>">
                                    <figcaption dir="rtl" class="thaana waheed"><?php echo $caption;  ?>
                                    </figcaption>
                                </figure>
                        <?php
                                    endif;
                                endforeach;
                            endif; 
                        ?>
                    </div>
                    <!-- THUMBNAILS -->
                    <div class="slider-nav-thumbnails">
                    <?php echo $nav; ?>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>


            
    
    
  

    <script src="<?php bloginfo('template_url');?>/js/app.js"></script>
    <script>
        // window._flux = new flux.slider('#sliderxx',{
        //     width: '100vw',
        //     pagination: true, 
        //     transitions: ['slide'], 
        //     captions: true
        // });
        $(document).on('ready', function () {
           
           $('.slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: false,
                asNavFor: '.slider-nav-thumbnails',
                autoplay: true,
                infinite: true
             });

             $('.slider-nav-thumbnails').slick({
                slidesToShow: 5,
                // slidesToScroll: 1,
                asNavFor: '.slider',
                // dots: true,
                centerMode: true,
                arrows: false,
                focusOnSelect: true
             });

             // Remove active class from all thumbnail slides
             $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

             // Set active class to first thumbnail slides
             $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

             // On before slide change match active thumbnail to current slide
             $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                var mySlideNumber = nextSlide;
                $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
                $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
            });
        });
    </script>

 <!--  <script type="text/javascript">
    $().ready(function (e) {

    $('.slickslide').not('.slick-initialized').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: false,
        slide: 'li',
        cssEase: 'linear',
        centerMode: true,
        slidesToShow: 1,
        // variableWidth: true,
        autoplay: true,
        autoplaySpeed: 4000,
        // responsive: [{
        //     breakpoint: 800,
        //     settings: {
        //         arrows: false,
        //         centerMode: false,
        //         centerPadding: '40px',
        //         variableWidth: false,
        //         slidesToShow: 1,
        //         dots: true
        //     },
        //     breakpoint: 1200,
        //     settings: {
        //         arrows: false,
        //         centerMode: false,
        //         centerPadding: '40px',
        //         variableWidth: false,
        //         slidesToShow: 1,
        //         dots: true

        //     }
        // }],
        // customPaging: function (slider, i) {
        //     return '<button class="tab">' + $('.slick-thumbs li:nth-child(' + (i + 1) + ')').html() + '</button>';
        // }
    });

    //$('.slick-thumbs').html('');
    //$('.slick-dots').appendTo('.slick-thumbs');
});
  </script> -->
</body>
</html>