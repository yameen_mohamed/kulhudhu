<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

get_header(); ?>
<style type="text/css">
	figcaption.wp-caption-text {
	    background: #ffe7e5;
	    padding: 0 13px;
	    border-right: 2px solid #f44336;
	    margin-top: 10px;
	}
</style>
<div id="primary" class="content-area ">
	<div class="row well">
		<div class="row">
			<div class="col-md-8">
				<main id="main" class="site-main" role="main">
					<?php
					// Start the loop.
					while ( have_posts() ) : the_post();

						// Include the single post content template.
						get_template_part( 'template-parts/content', 'single' );

						$tags = wp_get_post_tags($post->ID);
						if ($tags):
							?>

							<?php
							$first_tag = $tags[0]->term_id;
							$args=array(
								'tag__in' => array($first_tag),
								'post__not_in' => array($post->ID),
								'posts_per_page'=>5,
								'caller_get_posts'=>1
							);
							$my_query = new WP_Query($args);
							if( $my_query->have_posts() ) :
								?>
								<hr>
								<h4 class="waheed">ގުޅުން ހުރި ލިޔުންތައް</h4>
								<?php
								while ($my_query->have_posts()) : $my_query->the_post(); ?>
									<div class="media">
										<div class="media-body">
											<a href="<?php the_permalink() ?>">
												<h4 class="waheed media-heading"><?php the_title() ?></h4>
											</a>

										</div>
									</div>
									<?php
								endwhile;
							endif;
							wp_reset_query();
						endif;

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) {
							comments_template();
						}

						//for use in the loop, list 5 post titles related to first tag on current post


						if ( is_singular( 'attachment' ) ) {
							// Parent post navigation.
							the_post_navigation( array(
								'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'kulhudhufushi' ),
							) );
						} elseif ( is_singular( 'post' ) ) {
							// Previous/next post navigation.
							?>
							<hr>
							<div class="row ">
								<?php $prev = get_previous_post(); $next = get_next_post(); ?>
								<?php if($prev): ?>
								<div class="col-md-6">
									<div class="media">
										<div class="media-left">
											<a href="#">
												<?php echo get_the_post_thumbnail($prev->ID,[100,100]);?>
											</a>
										</div>
										<div class="media-body">
											<a href="<?php echo get_permalink($prev->ID) ?>"><h4 class="waheed media-heading"> <?php echo $prev->post_title;  ?> </h4></a>
										</div>
									</div>
								</div>
							<?php endif; ?>
							<?php if($next): ?>

								<div class="col-md-6">
									<div class="media">
										<div class="media-left">
											<a href="#">
												<?php echo get_the_post_thumbnail($next->ID,[100,100]);?>
											</a>
										</div>
										<div class="media-body">
											<a href="<?php echo get_permalink($next->ID) ?>"><h4 class="waheed media-heading"> <?php echo $next->post_title;  ?> </h4></a>
										</div>
									</div>
								</div>
								<?php endif; ?>
							</div>

							<?php
//							the_post_navigation( array(
//								'next_text' => '<span class="meta-nav" aria-hidden="true">' . '</span> ' .
//									'<span class="screen-reader-text">'  . '</span> ' .
//									'<span class="post-title">%title</span>',
//								'prev_text' => '<span class="meta-nav" aria-hidden="true">' . '</span> ' .
//									'<span class="screen-reader-text">'  . '</span> ' .
//									'<span class="post-title">%title</span>',
//								'screen_reader_text' => __( 'Post navigation','kulhudhufushi' )
//
//							) );

						}

						// End of the loop.
					endwhile;
					?>

				</main><!-- .site-main -->
			</div>
			<div class="col-md-4">
				<div class="side-advertisement">
					<?php the_advertisment('news-page-one') ?>
				</div>
				<div class="side-advertisement">
					<?php the_advertisment('news-page-two') ?>
				</div>
				<div>
					<div class="fb-page" data-href="https://www.facebook.com/kulhudhuffushionline" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/kulhudhuffushionline"><a href="https://www.facebook.com/kulhudhuffushionline">Kulhudhuffushi.com</a></blockquote></div></div>

				</div>

				<div>
					<br>

					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>

		<?php get_sidebar( 'content-bottom' ); ?>
	</div>


</div><!-- .content-area -->


<?php get_footer(); ?>
