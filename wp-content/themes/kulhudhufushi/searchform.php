<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text">
			ހޯދާ
		</span>
		<input type="search" class="search-field" placeholder="ހޯދާ" value="<?php echo get_search_query(); ?>" name="s" title="ހޯދާ" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text">ހޯދާ</span></button>
</form>
