// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify-css')

    ;

// create a default task and just log a message
gulp.task('default', function() {
    gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './bower_components/bootstrap/dist/js/bootstrap.js',
        './bower_components/bootstrap-material-design/dist/js/material.js',
        './bower_components/bootstrap-rtl/dist/js/holder.js',
        './bower_components/datetimepicker/build/jquery.datetimepicker.full.js',
        './bower_components/slick-carousel/slick/slick.js',
        './bower_components/local/js/app.js',

    ])
        .pipe(concat('app.js'))
        // .pipe(uglify())
        .pipe(gulp.dest('js'));

});


gulp.task('styles', function(){
    gulp.src([
        './bower_components/bootstrap/dist/css/bootstrap.css',
        './bower_components/bootstrap-flat/css/bootstrap-flat.css',
        './bower_components/bootstrap-material-design/dist/css/material.css',
        './bower_components/bootstrap-material-design/dist/css/ripples.css',
        './bower_components/bootstrap-material-design/dist/css/material-fullpalette.css',
        './bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.css',
        './bower_components/google-material-icons/dist/material-icons-font.css',
        './bower_components/datetimepicker/jquery.datetimepicker.css',
        './bower_components/slick-carousel/slick/slick.css',
        './bower_components/slick-carousel/slick/slick-theme.css',
        './bower_components/local/css/style.css'
    ])
        .pipe(concat('app.css'))
        //.pipe(minify())
        .pipe(gulp.dest('css'));

    gulp.src([
        './bower_components/google-material-icons/dist/material-icons-font.css',
        './bower_components/datetimepicker/jquery.datetimepicker.css',
    ])
        .pipe(concat('admin-app.css'))
        //.pipe(minify())
        .pipe(gulp.dest('css'));
    gulp.src(['./bower_components/google-material-icons/fonts/MaterialIcons-Regular.*',
        './bower_components/slick-carousel/fonts/slick.*'
        ])
        .pipe(gulp.dest('./fonts'));
});