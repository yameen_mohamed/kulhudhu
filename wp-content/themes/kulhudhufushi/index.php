<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */
$offset = 0;
get_header();

$onSlider = new WP_Query(array(
    'post_status' => 'publish',
    'meta_query' => array(
        array(
            'key'=>'show_on_carousel',
            'value'=>'true'
        )
    ),
));

$post_ids = wp_list_pluck( $onSlider->posts, 'ID' );


?>
<div class="">
    <div class="" style="margin-bottom: -10px">
        <?php include_once("template-parts/partials/home/home_under_nav_advertisement.php"); ?>
    </div>
</div>
<div class="row">
<div id="primary" class="content-area">
    <main id="main" class="container-fluid site-main" role="main">

        <div class="well">
            <div class="row">
                <div class="col-md-5">
                    <?php
                    include('template-parts/partials/carousel/carousel_slider.php');
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                    include('template-parts/partials/carousel/carousel_side_news.php');
                    ?>
                </div>
                <div class="col-md-3  ">
                    <div class="side-advertisement">
                        <?php the_advertisment('carouselnext') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php include('template-parts/partials/home/home_reports.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_news.php'); ?>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-6">
                    <div class="side-advertisement">
                        <?php the_advertisment('belownavigationfirst') ?>
                    </div>
                </div>
                <div class="col-md-6 ">
                    <div class="side-advertisement">
                        <?php the_advertisment('belownavigationsecond') ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php include('template-parts/partials/home/sports_area.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_second_row_left.php'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_column.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_trade.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_famous.php'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_technology.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_entertainment.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/home_second_row_left.php'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php include('template-parts/partials/home/videos.php'); ?>
            </div>
            <div class="col-md-4">
                <?php include('template-parts/partials/home/gallery.php'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?php include('template-parts/partials/home/home_religion.php'); ?>
            </div>
            <div class="col-md-3">
                <?php include('template-parts/partials/home/home_story.php'); ?>
            </div>
            <div class="col-md-3">
                <?php include('template-parts/partials/home/home_health.php'); ?>
            </div>
            <div class="col-md-3">
                <?php include('template-parts/partials/home/home_life_styles.php'); ?>
            </div>
        </div>
        

    </main>
    <!-- .site-main -->
</div><!-- .content-area -->
</div>
<?php get_footer(); ?>
