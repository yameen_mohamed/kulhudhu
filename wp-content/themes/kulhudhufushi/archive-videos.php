<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

get_header();
wp_enqueue_style('style', get_stylesheet_uri()); ?>
<style>
    .page-numbers {
        list-style: none;
        font-size: 12px;
        direction: rtl
    }

    .page-numbers li {
        display: inline;
    }

    .page-numbers li a {
        display: block;
        float: left;
        padding: 4px 9px;
        margin-right: 7px;
        border: 1px solid #efefef;
    }

    .page-numbers li span.current {
        display: block;
        float: left;
        padding: 4px 9px;
        margin-right: 7px;
        border: 1px solid #efefef;
        background-color: #f5f5f5;
    }

    .page-numbers li span.dots {
        display: block;
        float: left;
        padding: 4px 9px;
        margin-right: 7px;
    }
    .col-md-12.full-width iframe {
        width: 100%;
        height: 300px;
    }
</style>
<div id="primary" class="content-area row well ">
    <div class="row">
        <div class="col-md-8">
            <main id="main" class="site-main" role="main">
                <?php if (have_posts()) : ?>

                    <header class="page-header">
                        <h1 class="page-title waheed">
                            ވީޑިއޯ ގެލެރީ
                        </h1>

                        <div class="taxonomy-description"><?php the_archive_description() ?></div>
                    </header><!-- .page-header -->

                    <?php
                    // Start the Loop.
                    $index = 0;
                    while (have_posts()) : the_post();

                        if ($index == 0):
                    ?>
                        <div class="col-md-12 full-width">
                        <?php
                        get_template_part('template-parts/content', 'videos');

                        ?>

                            </div>
                            <hr>
                        <?php
                        
                        else:
                            ?>
                            <div class="col-md-6">
                        <?php get_template_part('template-parts/content', 'videos'); ?>

                        </div>
                        <?php
                        endif;
                        // End the loop.
                        $index++;
                    endwhile;

                // Previous/next page navigation.
//                        the_posts_pagination( array(
//                            'prev_text'          => __( 'Previous page', 'kulhudhufushi' ),
//                            'next_text'          => __( 'Next page', 'kulhudhufushi' ),
//                            'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'kulhudhufushi' ) . ' </span>',
//                        ) );


                // If no content, include the "No posts found" template.
                else:
                    get_template_part('template-parts/content', 'none');

                endif;
                ?>

            </main>
            <!-- .site-main -->
            <div class="clearfix"></div>
            <div style="direction: rtl">
                <?php
                global $wp_query;
                $total = $wp_query->max_num_pages;
                // only bother with the rest if we have more than 1 page!
                if ($total > 1) {
                    // get the current page
                    if (!$current_page = get_query_var('paged'))
                        $current_page = 1;
                    // structure of "format" depends on whether we're using pretty permalinks
                    if (!get_option('permalink_structure')) {
                        $format = '&paged=%#%';
                    } else {
                        $format = '/page/%#%/';
                    }
                    ?>

                    <?php
//                    echo paginate_links(array(
//                        'prev_text' => __('Previous page', 'kulhudhufushi'),
//                        'next_text' => __('Next page', 'kulhudhufushi'),
//                        'base' => get_pagenum_link(1) . '%_%',
//                        'format' => $format,
//                        'current' => $current_page,
//                        'total' => $total,
//                        'mid_size' => 2,
//                        'type' => 'list'
//                    ));

                    if (function_exists("wp_bs_pagination"))
                    {
                        //wp_bs_pagination($the_query->max_num_pages);
                        wp_bs_pagination();
                    }
                }
                ?>
            </div>

        </div>
        <div class="col-md-4">
            <div class="side-advertisement">
                <?php the_advertisment('listing-page') ?>
            </div>
            <div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>

</div><!-- .content-area -->


<?php get_footer(); ?>
