<style>
    .thumbnail
    {
        max-width: 300px;
        padding: 2px;
        transition: box-shadow .3s ease-in;
        display: inline-block;
        position: relative;
        overflow: hidden;
    }
    .thumbnail img {
        max-width: 100%;
        padding: 0;
        margin-bottom: -5px;
    }

    .thumbnail:after {
        content: attr(title);
        width: calc(100% - 24px);
        background: rgba(88, 79, 79, 0.75);
        padding: 10px;
        position: absolute;
        top: 100%;
        color: #fff;
        transition: bottom .2s ease-in;
        transition: top .2s ease-in;
    }

    .thumbnail:hover:after
    {
        bottom: 2px;
        top: auto;
    }

    .thumbnail:hover
    {
        box-shadow: #39080c 0 0 2px;
    }
    .thumbnail .delete {
        position: absolute;
        top: 8px;
        right: 8px;
        border-radius: 100%;
        background: #ccc;
        width: 18px;
        height: 18px;
        text-align: center;
        transition: background .3s ease-out;
        cursor: pointer;
    }
    .thumbnail .delete:hover {
        background: #BD0000;
        color: #fff;
    }
</style>
<p>
    <label for="image">Pick an image file</label><br>
    <input id="image" type="file" name="image[]" multiple="true" size="55"/><br/>
    <small>Supported png, jpeg, jpg and gif</small>
</p>
<p><label for="desc">A brief description</label><br>
    <input id="desc" type="text" name="description"/><br/>
</p>

<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 6/4/16
 * Time: 9:15 AM
 */



$image_count = get_post_meta($post->ID,'image_count',true);
if($image_count > 0):
    echo "<h2 class='hndle ui-sortable-handle'><span>Images</span></h2>";
    for($i=0; $i < $image_count;$i++):
?>
        <div class="thumbnail" title="<?php echo get_post_meta($post->ID,'image_desc_'.($i+1), true) ?>">
            <img class="" src='<?php echo get_post_meta($post->ID,'image_'.($i+1), true) ?>' />
            <span data-id="<?php echo $post->ID; ?>" data-index="<?php echo $i+1; ?>" class="delete">x</span>
        </div>
<?php
    endfor;
    endif; ?>
<script>
    $(document).ready(function()
    {
        $('.delete').click(function()
        {
            var self = this;
            if(confirm('Are you sure you want to delete this image?'))
            {
                $.ajax({
                    type: 'post',
                    data: {
                        action: 'delete_image',
                        post_id: $(self).data('id'),
                        index: $(self).data('index'),
                    },
                    url: '/wp-admin/admin-ajax.php',
                    success: function(data)
                    {
                        $data = JSON.parse(data);

                        if($data.status)
                        {
                            $(self).parent().remove();
                        }
                        else
                        {
                            alert($data.message);
                        }
                    }
                }) ;
            }
        });
    });
</script>
