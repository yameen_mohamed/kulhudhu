<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 2/28/16
 * Time: 8:54 AM
 */
class Gallery
{


    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        add_action('admin_menu', array($this, 'create_meta_box_gallery'));

    }


    private function register_handlers()
    {
        register_post_type('galleries',
            array(
                'labels' => array(
                    'name' => __( 'Galleries' ),
                    'singular_name' => __( 'Gallery' ),
                    'add_new_item' => 'Add New Gallery'
                ),
                'public' => true,
                'supports' => array('title', 'media','excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri().'/libs/youtube/resources/icon.png',
                'show_ui'=>true,
                'rewrite' => array('slug' => 'galleries'),
            )
        );
        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this,'save'), 12);

        add_filter("manage_edit-galleries_columns", array($this,"override_default_columns"));
        add_action("manage_galleries_posts_custom_column", array($this,"push_galleries_columns"), 10,2);

        add_action( 'wp_ajax_delete_image', array($this, 'delete_image'));

    }

    function delete_image()
    {
        if($meta = delete_post_meta($_POST['post_id'],'image_'.$_POST['index']))
        {
            update_post_meta($_POST['post_id'], 'image_count', get_post_meta($_POST['post_id'],'image_count', true)-1);

            $image_indices = get_post_meta($_POST['post_id'],'image_indices', true);

            $indices = explode(',', $image_indices);

            $indices = array_diff( $indices, [$_POST['index']] );

            update_post_meta($_POST['post_id'], 'image_indices', implode(',', $indices));
        }
        echo json_encode(array(
            'status' => $meta,
            'message' => $meta ?: 'Something went wrong!'
        ));
        exit;
    }

    function add_form_file_support()
    {
        echo 'enctype="multipart/form-data"';
    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'image_count'	=>	'No of Images',
            'images'    => 'Thumnails'
        );
        return $columns;
    }
    function push_galleries_columns($column)
    {
        global $post;
        switch ($column) {
            case 'images':
                for($i=1;$i<=$post->image_count;$i++)
                {
                    $img = "image_".$i;
                    $img_desc = "image_desc_".$i;

                    echo "<img width='50' title='{$post->$img_desc}' src='{$post->$img}' /> ";
                }
                break;
            case 'image_count':
                echo $post->image_count ?: '-';
                break;
        }
    }

    function create_meta_box_gallery()
    {
        add_meta_box( 'new-meta-boxes-galleries', 'Add Image', array($this,'new_meta_boxes_gallery'), 'galleries', 'normal', 'high' );

    }

    function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    function save($id){
        global $post;

        $files = count($_FILES) > 0 ? $this->reArrayFiles($_FILES['image']) : [];
        
        if($post->post_type == 'galleries' && count($files) > 0)
        {
            $count = get_post_meta($id,'image_count', true);
            $image_indices = get_post_meta($id,'image_indices', true);
            if(is_null($count)) $count=0;
            $indices = [];

            if(!is_null($image_indices))
            {
                $indices = explode(',', $image_indices);
            }
        


            foreach ($files as $image) {


                $index = ++$count;
                array_push($indices, $index);
                $ret = wp_handle_upload($image, array('test_form'=>false));
                update_post_meta($id, 'image_count', ($index));
                update_post_meta($id, 'image_'.($index), $ret['url']);
                update_post_meta($id, 'image_desc_'.($index), $_POST['description']);
            }

            update_post_meta($id, 'image_indices', implode(',', $indices));


        }
    }

    function new_meta_boxes_gallery() {
        global $post;

        include 'resources/view.php';

    }
}

new Gallery();

