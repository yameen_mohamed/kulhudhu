<?php



class PhotoCaption {

	protected $factor;
	protected $image;
	protected $title;
	protected $font;
	protected $fontSize = 24;

	protected $file;

	protected $primary_image_handle;

	// public function __construct($image, $title, $font, $rtl = false)
	// {
	// 	add_action('save_post', array($this, 'save'), 12);
	// 	add_action( 'post_submitbox_misc_actions', 'custom_button' );

	// 	$this->image = $image;
	// 	$this->title = $rtl ?  $this->hebstrrev($title) : $title;
	// 	$this->font = __DIR__. '/mv_waheed.ttf';

	// }


	function custom_button(){
	        $html  = '<div id="major-publishing-actions" style="overflow:hidden">';
	        $html .= '<input type="checkbox" accesskey="p" tabindex="5" id="forceOg" name="force_og">';
	        $html .= '<label for="forceOg"> Force generate social image</label>';
	        $html .= '</div>';
	        echo $html;
	}


	public function __construct()
	{
		add_action('save_post', array($this, 'save'), 12);
		add_action( 'post_submitbox_misc_actions', array($this,'custom_button'));
        add_action('admin_menu', array($this, 'create_meta_box_og_image'));


		// $this->image = $image;
		// $this->title = $rtl ?  $this->hebstrrev($title) : $title;
		$this->font = __DIR__. '/mv_waheed.ttf';

	}


	function create_meta_box_og_image()
    {
        add_meta_box('breaking','OG Image',array($this,'og_image_box'),'post','side','high');
        add_meta_box('transliteration','Transliteration',array($this,'latin_box'),'post','side','high');

    }

    function og_image_box()
    {
        global $post;
        $og_image_url = get_post_meta($post->ID,'kulhudhu_social_image',true);

        if(strlen($og_image_url) > 1)
        	echo '<img style="width:100%" src='.$og_image_url.'?'.rand().'>';
        else
        	echo '<strong>We do not have a social image created yet! Shall we?</strong>';

    }

    function latin_box()
    {
        global $post;
        $og_image_url = get_post_meta($post->ID,'post_transliteration',true);

        echo '<textarea name="post_transliteration" class="wp-editor-area" placeholder="Latin Transliteration" style="width:100%" >'.$og_image_url.'</textarea>';

    }

	function imagecreatefromfile( $filename ) {
	    // if (!file_exists($filename)) {
	    //     throw new InvalidArgumentException('File "'.$filename.'" not found.');

	    // }
	    switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
	        case 'jpeg':
	        case 'jpg':
	            return imagecreatefromjpeg($filename);
	        break;

	        case 'png':
	            return imagecreatefrompng($filename);
	        break;

	        case 'gif':
	            return imagecreatefromgif($filename);
	        break;

	        default:
	            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
	        break;
	    }
	}
	function save($id){
        global $post;

        if($post->post_type == 'post')
        {
        	$ID =$post->ID;  
			$upload_dir   = wp_upload_dir();
		    
		    $this->image = get_the_post_thumbnail_url($ID, 'big');
        	
        	$og_image_url = get_post_meta($post->ID,'kulhudhu_social_image',true);

			$this->file = $upload_dir['basedir'].substr($og_image_url, strpos($og_image_url, 'uploads')+7);

			// var_dump($_POST['post_transliteration']);die;

		    $this->title = $this->hebstrrev($post->post_title);

		    $update = strlen($og_image_url) > 1;

		    if(!file_exists($this->file) || isset($_POST['force_og']))
		    {

		    	if(is_file($this->file) ){
		    		@unlink($this->file);
		    	}

				$this->file = $upload_dir['basedir'].'/'.md5(date('Y-m-d i:a')).'.png';

        		$this->MakeImage();

		    }
            
            if ( ! add_post_meta( $ID, 'post_transliteration', $_POST['post_transliteration'], true ) ) { 
			   update_post_meta( $ID, 'post_transliteration', $_POST['post_transliteration'] );
			}
            
            if($update)
            	update_post_meta($ID, 'kulhudhu_social_image', $this->abs_path_to_url($this->file));
           	else
            	add_post_meta($ID, 'kulhudhu_social_image', $this->abs_path_to_url($this->file));

        }
    }

    function abs_path_to_url( $path = '' ) {
	    $url = str_replace(
	        wp_normalize_path( untrailingslashit( ABSPATH ) ),
	        site_url(),
	        wp_normalize_path( $path )
	    );
	    return esc_url_raw( $url );
	}
    private function properText($text){

	    // Convert UTF-8 string to HTML entities
	    $text = mb_convert_encoding($text, 'HTML-ENTITIES',"UTF-8");
	    // Convert HTML entities into ISO-8859-1
	    $text = html_entity_decode($text,ENT_NOQUOTES, "ISO-8859-1");
	    // Convert characters > 127 into their hexidecimal equivalents
	    $out = "";
	    for($i = 0; $i < strlen($text); $i++) {
	        $letter = $text[$i];
	        $num = ord($letter);
	        if($num>127) {
	          $out .= "&#$num;";
	        } else {
	          $out .=  $letter;
	        }
	    }

	    return $out;

	}

	private function hebstrrev($string, $revInt = false, $encoding = 'UTF-8'){
		$mb_strrev = function($str) use ($encoding){return mb_convert_encoding(strrev(mb_convert_encoding($str, 'UTF-16BE', $encoding)), $encoding, 'UTF-16LE');};
		if(!$revInt){
			$s = '';
			foreach(array_reverse(preg_split('/(?<=\D)(?=\d)|\d+\K/', $string)) as $val){
				$s .= ctype_digit($val) ? $val : $mb_strrev($val);
			}
			return $s;
		} else {
			return $mb_strrev($string);
		}
	}


	public function MakeImage()
	{
		$this->primary_image_handle = $this->imagecreatefromfile($this->image);


		$dimensions = imagettfbbox($this->fontSize, 0, $this->font, $this->title);



		// die($this->title);
		
		$sx = imagesx($this->primary_image_handle);
		$sy = imagesy($this->primary_image_handle);
		$this->factor = 200; 



		$stamp = imagecreatetruecolor(920,480);
		$green = imagecolorallocate($stamp, 0, 136, 66);
		$white = imagecolorallocate($stamp, 255, 255, 255);


		imagefilledrectangle($stamp, 0, 0, 920, 480, $green);

		$icon = imagecreatefrompng( __DIR__. '/icon.png');

		$margin = 10;
		$text = array_reverse(explode("\n", wordwrap($this->title, 80)));


		$delta_y = 260 + ((260 - (count($text)*56))/2);
		$y = (imagesy($stamp) - (($dimensions[1] - $dimensions[7]) + $margin)*count($text)) * (1/1.3);



		$iconW = imagesx($icon);
		$iconH = imagesy($icon);

		$cropX = ($sx - 650)/2;

		$cropped = imagecrop($this->primary_image_handle, ['x' => $cropX, 'y' => 0, 'width' => 650, 'height' => 480]);

		imagecopyresampled($stamp, $cropped, 0,0,0,0, 650,480,650,460);
		imagecopyresampled($stamp, $icon, 480 + 40 + ( ($this->factor) - ($iconW/2)) , 120,0,0, $iconW, $iconH, $iconW, $iconH);

		$points = array(
            650,  0, 
            650,  480,
            370,  480, 
        );

        imagefilledpolygon($stamp, $points, 3, $green);
		imagesetthickness($stamp, 4);
		imageline($stamp, 565 ,  230,  920 - 50 ,  230, $white);



		foreach($text as $line) {
			$box = imagettfbbox($this->fontSize, 0, $this->font, $line);

			$width = abs($box[4] - $box[0]);
			$height = abs($box[5] - $box[1]);

			$x = 565 + ((290-$width)/2);


			// var_dump($box, $width, $height);die;

			// $x = (480 - ($dimensions[4] - $dimensions[6])) / 2;
			imagettftext($stamp, $this->fontSize, 0,  $x, $delta_y, $white, $this->font, $line);
			$delta_y =  $delta_y + $height + $margin;


        	// imagefilledpolygon($stamp, $box, count($dimensions)/2, $white);

		}

		// imagesetthickness($stamp, 10);

		// imageline($stamp, 670 ,  0,  385 ,  480, $white);
		
		imagepng($stamp, $this->file);
		// imagepng($stamp, './stamped.png');
		imagedestroy($stamp);
		imagedestroy($this->primary_image_handle);

	}

	function random_string($length) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));

	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }

	    return $key;
	}

}

        


// $pc = new PhotoCaption('//Users//yameen//Desktop//humaidha.png','ރޯގާތަކަކީ އެއަށް ބޭހެއް ނެތް ނަމަވެސް ނިކަން އަވަހަށް ފިލައިގެންދާ ރޯގާ ތަކެކެވެ.','waheed.ttf', true);
// $pc->MakeImage();
$pc = new PhotoCaption();

?>

