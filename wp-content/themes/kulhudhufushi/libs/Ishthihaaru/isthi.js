jQuery(document).ready( function($) {

      $('.remove_ad').on('click', function()
        {
            var self = this;
            $.ajax({
                type: 'post',
                data: {
                    action: 'remove_ad',
                    ad_id: $(self).data('ad_id'),
                    id: $(self).data('id'),
                },
                url: '/wp-admin/admin-ajax.php',
                success: function(data)
                {
                    $(self).parent().remove();
                }
            }) ;
        });

      jQuery('#upload_logo_button').click(function(e) {

             e.preventDefault();
             var image_frame;
             if(image_frame){
                 image_frame.open();
             }
             // Define image_frame as wp.media object
             image_frame = wp.media({
                           title: 'Select Media',
                           multiple : false,
                           library : {
                                type : 'image',
                            }
                       });

                       image_frame.on('close',function() {
                          // On close, get selections and save to the hidden input
                          // plus other AJAX stuff to refresh the image preview
                          var selection =  image_frame.state().get('selection');
                          var gallery_ids = new Array();
                          var my_index = 0;
                          selection.each(function(attachment) {
                          	console.log(attachment.changed.url);

                          	$('#adzone').append("<div class=ads> <img src=" 
                              +attachment.changed.url+ 
                              "> <input type=hidden name='ad[]' value="+attachment.changed.url + 
                              " <div><input placeholder='Link for the ad' style='width:100%' type=text name='links[]'></div>" +
                              " </div>");
                             gallery_ids[my_index] = attachment['id'];
                             my_index++;
                          });
                          var ids = gallery_ids.join(",");
                          jQuery('input#myprefix_image_id').val(ids);

                          console.log(ids);
                          Refresh_Image(ids);
                       });

                      image_frame.on('open',function() {
                        // On open, get the id from the hidden input
                        // and select the appropiate images in the media manager
                        var selection =  image_frame.state().get('selection');
                        var  mypref = jQuery('input#myprefix_image_id').val();
                        if(mypref)
                        {
	                        ids = mypref.split(',');
	                        ids.forEach(function(id) {
	                          attachment = wp.media.attachment(id);
	                          attachment.fetch();
	                          selection.add( attachment ? [ attachment ] : [] );
	                        });
                        }

                      });

                    image_frame.open();
     });

});

// Ajax request to refresh the image preview
function Refresh_Image(the_id){
        var data = {
            action: 'myprefix_get_image',
            id: the_id
        };

        jQuery.get(ajaxurl, data, function(response) {

            if(response.success === true) {
                jQuery('#myprefix-preview-image').replaceWith( response.data.image );
            }
        });
}