<?php


class Ishthihaaru
{
	/**
     * Advertisement constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        $this->ishthihaaru_taxonomy();
        add_action('admin_menu', array($this, 'create_meta_box_video'));
		add_action('admin_enqueue_scripts', array($this,'queueUpScripts'));

        add_shortcode('ad', array($this,'load_shortcode'));

    	// add_action('admin_menu', array($this,'test_plugin_setup_menu'));

    }

 
	function test_plugin_setup_menu(){
	        add_menu_page( 'Test Plugin Page', 'Test Plugin', 'manage_options', 'ishthihaaru', 'test_init' );
	}

    function load_shortcode($arg) {
        $ad = Ishthihaaru::getRandomAdvertisement($arg['cat']);
        return sprintf("<div class='side-advertisement'><a href='%s' target='_blank'><img class='advertisement' src='%s'></a></div>",$ad['link'],$ad['image'] );
        // the_advertisment(  );
    }

   


    private function register_handlers()
    {
        register_post_type('ishthihaaru',
            array(
                'labels' => array(
                    'name' => __('Ishthihaaru'),
                    'singular_name' => __('Ishthihaaru'),
                    'add_new_item' => 'Add New Ishthihaaru'
                ),
//                'taxonomies' => array('category'),
                'public' => true,
                'supports' => array('title','media'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri() . '/images/adv.png',
                'show_ui' => true,
                'rewrite' => array('slug' => 'ishthihaaru'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this, 'save'), 12);

        add_filter("manage_edit-ishthihaaru_columns", array($this, "override_default_columns"));
        add_action("manage_ishthihaaru_posts_custom_column", array($this, "push_ishthihaaru_columns"), 10, 2);


    }

    function ishthihaaru_taxonomy() {
        register_taxonomy(
            'ishthihaaru_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'ishthihaaru',        //post type name

            array(
                    'add_new_item' => 'Add New Ishthihaaru',
                'hierarchical' => true,
                'label' => 'Ishthihaaru Types',  //Display name
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'ishthihaaru', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                    'hierarchical' => true
                )
            )
        );
    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'category'  => 'Categories',
            'image'	=>	'Advertisement Preview',
        );
        return $columns;
    }
    function push_ishthihaaru_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                $imageStr = get_post_meta($post->ID,'ad_'.$post->ID,true);
                $linkStr = get_post_meta($post->ID,'ad_links_'.$post->ID,true);
                $images = explode(';', $imageStr);
                $links = explode(';', $linkStr);

                foreach ($images as $index => $img) {
                    echo sprintf("<img class='ad' width='100' title='%s' src='%s' >", $links[$index], $img);
                }

                break;
            case 'category':
                echo "<ol>";
                foreach (wp_get_post_terms($post->ID,'ishthihaaru_categories') as $term) {
                    echo "<li>{$term->name}</li>";
                }
                echo "</ol>";

                break;
        }
    }


    function save($id){
        global $post;
        if($post->post_type == 'ishthihaaru')
        {
            // var_dump(implode(";", $_POST['links']));die;
            update_post_meta($id, 'ad_'.$post->ID , implode(";", $_POST['ad']));
            update_post_meta($id, 'ad_links_'.$post->ID , implode(";", $_POST['links']));
            // update_post_meta($id, 'ad_link_'.$post->ID , $_POST['adlink']);
        }
    }


    function create_meta_box_video()
    {
        add_meta_box( 'new-meta-boxes-videos', 'Ads', array($this,'new_meta_boxes_video'), 'ishthihaaru', 'normal', 'high' );
    }

    function queueUpScripts() {
    	wp_register_script( 'isthi-js', get_template_directory_uri() .'/libs/Ishthihaaru/isthi.js?v=0.1', array('jquery','media-upload','thickbox') );
    	wp_register_style( 'isthi-css', get_template_directory_uri() .'/libs/Ishthihaaru/isthi.css?v=0.1', array() );
	 	wp_enqueue_media();
        wp_enqueue_script('isthi-js');
        wp_enqueue_style('isthi-css');
	 
	}

    function new_meta_boxes_video() {
        global $post;
        $adLink = get_post_meta($post->ID,'ad_link_'.$post->ID,true);


        echo'<button class="preview button" id=upload_logo_button type="button" name="embed_code" size="55" >
<i class="wp-menu-image dashicons-before dashicons-admin-post"></i>
        Add Image</button><br>';
        echo "<div id=adzone>";
        $meta_box_value = get_post_meta($post->ID,'ad_'.$post->ID,true);
        $link_string = get_post_meta($post->ID,'ad_links_'.$post->ID,true);

        if($meta_box_value) { 
            $links = explode(';', $link_string);
        	foreach (explode(';', $meta_box_value) as $index=>$img) {
                
                $link = $links[$index];
        		
		        echo "<div class=ads>
                        <a class='delete remove_ad' data-id='{$post->ID}' data-ad_id='$img' class=delete >X</a>
		        		<img  src='$img' >
                        <input type=hidden name='ad[]' value='$img' ><br>
		        		<div><input placeholder='Link for the ad' style='width:100%' type=text name='links[]' value='$link' ></div>
		        	</div>
		        ";
        	}
        	// echo html_entity_decode($post->embed_code); 
        }
        echo "</div>";
    }


    public static function getRandomAdvertisement($term)
    {
        $args = array (
            'post_type'             => 'ishthihaaru',
            'nopaging'               => false,
            'posts_per_page'         => '1',
            'order'                  => 'DESC',
            'orderby'                => 'id',
            'tax_query' => array(
                array(
                    'taxonomy' => 'ishthihaaru_categories',
                    'field'    => 'slug',
                    'terms'    => $term,
                ),
            ),
        );

        $query = new WP_Query( $args );
        
        if($query->have_posts()){
            if($query->post)
            {
        		$adLink = get_post_meta($query->post->ID,'ad_links_'.$query->post->ID,true);
        		$imageStr = get_post_meta($query->post->ID,'ad_'.$query->post->ID,true);
                $images = explode(';', $imageStr);
                $links = explode(';', $adLink);
                $index = rand(0,count($images)-1);

                return array('image' => $images[$index] , 'link' => $links[$index]);
            }
        }
        return;
    }
}


new Ishthihaaru();