<div class="panel panel-material-red">
    <div class="panel-heading waheed">
        <a href="<?php echo get_term_link('news', 'category') ?>">
            ފަހުގެ ޚަބަރު
        </a>
    </div>
    <div class="panel-body">
        <div class="list-group">
            <?php
            $posts = new WP_Query(array('posts_per_page' => $instance['number']));
            $index=0;
            while ($posts->have_posts()) : $posts->the_post();

            ?>
            <div class="list-group-item">
                <?php if ($index == 0 && has_post_thumbnail()): ?>
                    <div class="list-group-cover">
                        <img src="<?php echo get_the_post_thumbnail_url() ?>" alt="...">
                    </div>
                <?php endif; ?>
                <div class="row-content">
                    <div class="action-secondary"><i class="mdi-material-info"></i></div>
                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                        <h4 class="list-group-item-heading"><?php the_title(); ?></h4>
                    </a>

                    <!--                    <p class="list-group-item-text">--><?php //the_excerpt()
                    ?><!--</p>-->
                </div>
            </div>
            <div class="list-group-separator"></div>
            <?php $index++; endwhile; wp_reset_query(); ?>
        </div>
    </div>
</div>