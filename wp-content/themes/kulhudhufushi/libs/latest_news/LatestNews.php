<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 4/1/16
 * Time: 4:31 AM
 */
class LatestNews extends WP_Widget
{
    function __construct()
    {

        parent::__construct(
            'lastest_news_widget',
            __('Latest News', 'kulhudhuffushi'),
            array('description' => __('Display latest news!', 'kulhudhuffushi'),)
        );
    }

    // Creating widget front-end
// This is where the action happens
    public function widget($args, $instance)
    {
        include 'views/news_listing.php';
        echo $args['after_widget'];
    }

// Widget Backend
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }

        if (isset($instance['number'])) {
            $number = $instance['number'];
        } else {
            $number = __('New number', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>"
                   name="<?php echo $this->get_field_name('number'); ?>" type="text"
                   value="<?php echo esc_attr($number); ?>"/>
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['number'] = (!empty($new_instance['number'])) ? strip_tags($new_instance['number']) : '';
        return $instance;
    }
}

// Register and load the widget
function load_service_widget() {
    register_widget( 'LatestNews' );
}
add_action( 'widgets_init', 'load_service_widget' );