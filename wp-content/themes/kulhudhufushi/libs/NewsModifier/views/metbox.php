<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 4/1/16
 * Time: 1:40 AM
 */

?>
<p>
    <label for="slide_upload">Expiry Date</label><br>
    <input style="width: 100%" id="breaking_expiry_date" type="text" name="breaking_expiry_date" placeholder="Expiry Date" value="<?php echo $post->breaking_expiry_date; ?>" />
    <br />
    <small>Expiry date is upto what the breaking state sustains!</small>
</p>

<script>
    $(document).ready(function(){
        jQuery('#breaking_expiry_date').datetimepicker();
    });
</script>