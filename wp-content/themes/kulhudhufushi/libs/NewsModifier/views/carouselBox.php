<input id="show_on_carousel" <?php echo $meta_box_value ? 'checked' : '' ?> name="show_on_carousel" value="true" type="checkbox">
<label for="show_on_carousel">Show this article on carousel</label><br>
<small>However, you will need the featured image set in order for this to be visible on the carousel.</small>
