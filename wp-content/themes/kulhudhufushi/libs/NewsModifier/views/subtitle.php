<?php
/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 4/1/16
 * Time: 1:40 AM
 */

?>
<style>
    .sub-titles li input {
        display: inherit;
    }
    .btn-add
    {
        color: #a00;
        text-decoration: underline;
    }
</style>

<ol class='sub-titles' id='title-holder'>
<?php if($meta_box_value): ?>
<a class='btn-add' data-index="<?= $meta_box_value ?>" style="float: right;margin-bottom: 10px;" id='new-title'>Add New</a>

<?php
    for ($i=1; $i <= $meta_box_value; $i++) { 
?>
<li>
    <input style="width: 100%" id="title-<?= $i ?>" type="text" name="titles[<?= $i ?>]" placeholder="Sub Title <?= $i ?>" value="<?=  get_subtitle($post->ID, $i); ?>" />
</li>
<?php
    }    
?>

<?php else: ?>
<center>
    <a class='btn-add' id='new-title'>Add New</a>
</center>
<?php endif; ?>
</ol>


<script>
    $(document).ready(function(){
        jQuery('#breaking_expiry_date').datetimepicker();
        jQuery('#new-title').on('click', function(){
            $(this).hide();
            $index = 1;
            if($(this).data('index'))
            {
                $index = $(this).data('index');
            }

            jQuery('#title-holder')
                .append('<li><input style="width: 100%" id="title-1" type="text" name="titles['+$index+']" placeholder="Expiry Date" /></li>');
        })
    });
</script>