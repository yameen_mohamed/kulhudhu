<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 3/31/16
 * Time: 3:41 PM
 */
class NewsModifier
{


    protected $pusher;

    /**
     * NewsModifier constructor.
     */
    public function __construct()
    {
        $this->register_handlers();

        $app_id = "313146";
        $app_key = "89335e29a0cdde9d5bea";
        $app_secret = "0af045217ba405850929";
        $app_cluster = "ap2";

        $this->pusher = new Pusher\Pusher( 
            $app_key, $app_secret, $app_id, 
            array( 'cluster' => $app_cluster, 
                'encrypted' => true, 
                'curl_options' => array( CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4 
                ) 
            ) 
        );


    }

    private function register_handlers()
    {
        add_action('admin_menu', array($this, 'create_meta_box_breaking_new_box'));

        wp_enqueue_style('admin_custom', get_template_directory_uri(). '/css/admin-app.css');

        add_action('save_post', array($this, 'save'), 12);


        add_filter( 'manage_edit-post_columns', array($this,'price_column_register' ));

        add_action( 'manage_posts_custom_column', array($this,'price_column_display'), 1, 2);

    }

    function price_column_display($column_name, $post_id)
    {
        if ( 'other_attrs' != $column_name )
            return;

        $breaking_news_expiry = get_post_meta($post_id, 'breaking_expiry_date', true);
        $show_on_carousel = get_post_meta($post_id, 'show_on_carousel', true);

        if($breaking_news_expiry)
            printf("<span class='breaking' title='%s'><i class='material-icons'>trending_up</i></span>", "Breaking news expires on $breaking_news_expiry");
        if($show_on_carousel)
            echo "<span class='on_carousel' title='On Carousel'><i class='material-icons'>slideshow</i></span>";

    }

    function price_column_register( $columns ) {
        $columns['other_attrs'] = __( 'Modifiers');
        return $columns;
    }


    function create_meta_box_breaking_new_box()
    {
        add_meta_box('slider','Show On Carousel',array($this,'show_carousel_box'),'post','side','high');
        add_meta_box('breaking','Breaking News',array($this,'breaking_new_box'),'post','side','high');
        add_meta_box('notifications','Notifications',array($this,'notifications_new_box'),'post','side','high');
        add_meta_box('subtitles','Sub Titles',array($this,'subtitles_new_box'),'post','normal','high');
        add_meta_box('blocks','Breaking Blocks',array($this,'blocks_new_box'),'post','normal','high');
    }

    public function blocks_new_box(){
        global $post;
        $meta_box_value = get_post_meta($post->ID,'block_count',true);
        // if($meta_box_value) { echo html_entity_decode($post->breaking_expiry_date); }

        $newId = $meta_box_value ? $meta_box_value+1 : 1;
        echo "<h3>New Block</h3>";
        wp_editor( "", 'mettaabox_ID', $settings = array('textarea_name'=>  "blocks[$newId]") );
        
        $i = 1;

        if($meta_box_value)
        {
            for($i=1;$i<= $meta_box_value; $i++)
            {
                echo "<h3>Block #{$i}</h3>";
                $text= get_post_meta($post->ID, 'block-'.$i , true );
                $user_id = get_post_meta($post->ID, 'block-'.$i.'-user' , true );
                $user = get_user_by('ID', $user_id);
                if($user)
                    echo "<h4>Author: <i>$user->first_name $user->last_name </i></h4>";
                
                $date = get_post_meta($post->ID, 'block-'.$i.'-date' , true );
                if($date)
                    echo "<h4>Last Updated On: <i>$date </i></h4>";
                wp_editor( htmlspecialchars_decode($text), 'mettaabox_ID', $settings = array('textarea_name'=> "blocks[$i]") );
            }
        }


        include_once 'views/blocks.php';
    }

    public function subtitles_new_box(){
        global $post;
        $meta_box_value = get_post_meta($post->ID,'subtitle_count',true);
        // if($meta_box_value) { echo html_entity_decode($post->breaking_expiry_date); }

        include_once 'views/subtitle.php';
    }
    public function breaking_new_box(){
        global $post;
        $meta_box_value = get_post_meta($post->ID,'breaking_expiry_date',true);
        if($meta_box_value) { echo html_entity_decode($post->breaking_expiry_date); }

        include_once 'views/metbox.php';
    }
    public function notifications_new_box(){
        global $post;
        $meta_box_value = get_post_meta($post->ID,'send_notifications',true);

        if($meta_box_value == 'true')
            echo "Notified!";
        else
            include_once 'views/notifications.php';
    }

    public function show_carousel_box(){
        global $post;
        $meta_box_value = get_post_meta($post->ID,'show_on_carousel',true);
        include_once 'views/carouselBox.php';
    }

    function get_block($i)
    {
        global $post;
        return get_post_meta($post->ID,'block-'.$i,true);
    }

    function save($id){
        global $post;
        if($post->post_type == 'post')
        {
            if($_POST['titles'])
            {
                $titles = $_POST['titles'];

                foreach($titles as $index => $title)
                {
                    update_post_meta($id, 'title-'.$index, $title);
                }
                update_post_meta($id, 'subtitle_count', $index);

            }

            if($_POST['blocks'])
            {
                $titles = $_POST['blocks'];
                $counter = 0;
                foreach($titles as $index => $title)
                {
                    if(strlen($title) <= 0) continue;
                    if($index == 0) $index=1;
                    if($this->get_block($index) != $title)
                    {
                        update_post_meta($id, 'block-'.$index, $title);
                        update_post_meta($id, 'block-'.$index.'-user', get_current_user_id());
                        update_post_meta($id, 'block-'.$index.'-date', date('Y-m-d h:i:s a'));
                    }

                    $counter++;

                }
                update_post_meta($id, 'block_count', $counter);

            }

            if($_POST['breaking_expiry_date'] !='')
                update_post_meta($id, 'breaking_expiry_date', $_POST['breaking_expiry_date']);

            if($_POST['send_notifications'] !='')
            {
                $this->pusher->trigger( 'updates', 'notify', [
                    'name' => 'Kulhudhuffushi Online',
                    'icon' => 'https://kulhudhuffushi.com/wp-content/themes/kulhudhufushi/images/fav-ico.gif',
                    'message' => $post->post_title,
                    'url' => $post->post_perma_link

				] );
				
				die("pusing");
                update_post_meta($id, 'send_notifications', $_POST['send_notifications']);
            }
            update_post_meta($id, 'show_on_carousel', $_POST['show_on_carousel']);
        }
    }


}

function get_subtitle($postID, $idx)
{
    return get_post_meta($postID,'title-'.$idx,true);
}
new NewsModifier();