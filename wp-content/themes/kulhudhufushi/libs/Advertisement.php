<?php

/**
 * Created by PhpStorm.
 * User: yameen
 * Date: 3/18/16
 * Time: 2:05 PM
 */
class Advertisement
{
    /**
     * Advertisement constructor.
     */
    public function __construct()
    {
        $this->register_handlers();
        $this->advertisements_taxonomy();

    }


    private function register_handlers()
    {
        register_post_type('advertisements',
            array(
                'labels' => array(
                    'name' => __('Advertisements'),
                    'singular_name' => __('Advertisement'),
                    'add_new_item' => 'Add New Advertisement'
                ),
//                'taxonomies' => array('category'),
                'public' => true,
                'supports' => array('title', 'media', 'editor', 'excerpt'),
                'has_archive' => true,
                'menu_icon' => get_template_directory_uri() . '/images/adv.png',
                'show_ui' => true,
                'rewrite' => array('slug' => 'advertisements'),
            )
        );
//        add_action('post_edit_form_tag', array($this,'add_form_file_support'));
        add_action('save_post', array($this, 'save'), 12);

        add_filter("manage_edit-advertisements_columns", array($this, "override_default_columns"));
        add_action("manage_advertisements_posts_custom_column", array($this, "push_advertisements_columns"), 10, 2);


    }

    function advertisements_taxonomy() {
        register_taxonomy(
            'advertisements_categories',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
            'advertisements',        //post type name
            array(
                'hierarchical' => true,
                'label' => 'Advertisement Types',  //Display name
                'query_var' => true,
                'rewrite' => array(
                    'slug' => 'advertisements', // This controls the base slug that will display before each term
                    'with_front' => false, // Don't display the category base before
                    'hierarchical' => true
                )
            )
        );
    }

    function override_default_columns()
    {
        $columns = array(
            'cb'	 	=> '<input type="checkbox" />',
            'title' 	=> 'Title',
            'category'  => 'Category',
            'image'	=>	'Advertisement Preview',
        );
        return $columns;
    }
    function push_advertisements_columns($column)
    {
        global $post;
        switch ($column) {
            case 'image':
                $images = get_post_gallery_images();
                foreach ($images as $img) {
                    echo sprintf("<img width='100' src='%s' >", $img);
                }

                break;
            case 'category':
                echo get_terms('category')[0]->name;
                break;
        }
    }


    function save($id){
        global $post;
        if($post->post_type == 'advertisements' && $_POST['embed_code'] !='')
        {
            update_post_meta($id, 'embed_code', $_POST['embed_code']);
        }
    }


    public static function getRandomAdvertisement($term)
    {
        $args = array (
            'post_type'             => 'advertisements',
            'nopaging'               => false,
            'posts_per_page'         => '1',
            'order'                  => 'DESC',
            'orderby'                => 'id',
            'tax_query' => array(
                array(
                    'taxonomy' => 'advertisements_categories',
                    'field'    => 'slug',
                    'terms'    => $term,
                ),
            ),
        );

        $query = new WP_Query( $args );
        
        if($query->have_posts()){
            if($query->post)
            {
                $images = get_post_gallery_images($query->post->ID);
                return $images[rand(0,count($images)-1)];
            }
        }
        return;
    }


}

new Advertisement();