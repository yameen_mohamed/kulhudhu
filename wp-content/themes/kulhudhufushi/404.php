<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Kulhudhufushi
 */

get_header(); ?>


	<div id="primary" class="content-area row well">
		<div class="row">
			<div class="col-md-8">
				<main id="main" class="site-main" role="main">

					<section class="error-404 not-found">
						<header class="page-header">
							<h1 class="waheed page-title">
								ތިހޯއްދަވާ އެއްޗެއް ނުފެނުނު!
							</h1>
						</header><!-- .page-header -->

						<div class="page-content waheed">
							<p>
								ފަހަރުގައި ތިހޯއްދަވާ އެއްޗެއް ތިބައްލަވާތަނުގައި ނުއޮތުން އެކަށީގެންވޭ! ސާޗް ކޮއްލައްވަންވީނޫންތޯ!
							</p>

							<?php get_search_form(); ?>
						</div><!-- .page-content -->
					</section><!-- .error-404 -->

				</main><!-- .site-main -->
			</div>
			<div class="md-4">
				<?php get_sidebar( 'content-bottom' ); ?>
			</div>
		</div>

	</div><!-- .content-area -->
<?php get_footer(); ?>
