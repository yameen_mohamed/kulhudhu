<?php 
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'kulhudhu_ssl');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N~KybRQP4/5EjFEz3B`:[xT:@nB-gp){rv_h!Y4{#f9$,xLf&E%2`mWV`>`V}FYw');
define('SECURE_AUTH_KEY',  '>:bGc,_3h`m=SG nqhEBO/`RLSAgE^Sy*2Eb!:Y-|d5[{JOJh5[[[{aw]Ct^XSB ');
define('LOGGED_IN_KEY',    ':&A~T2%0ScJSu37s?6;sjaL=s3!m7e0Y(9R2{wWV0?~t# 5,J6T#YX{?wgB0NnoT');
define('NONCE_KEY',        'c{}5R{lFf1Bs9k5U4D]@W8S0[Fh<:UIrIOop)%T+s>8^wg46rmWXR8Mm)szGk1$g');
define('AUTH_SALT',        '205^JniB)~,0-Vj8m6RQx_J%3{vwG6K[j/,&_HMpP#leiigTaZSNAp)DzX[=7qua');
define('SECURE_AUTH_SALT', 'v<G?|8FgT)J8SOR/0#@IZvg*]-=b)3pRQaZ`DA;D=tFYbb=`j3y>x0lcT!}05.?R');
define('LOGGED_IN_SALT',   '-iG2y-j0U~ndrwsX6573MqF v%aI?dT.d!q_AV_nOHQe?<ezou4,r]ja+;(r[[y+');
define('NONCE_SALT',       '<W+GOJoaX/-G}so_$DbMD2<fU@wr^Q;B|!<%4K~1lOx/~|62,>0SF6v?|U;^h*?:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
